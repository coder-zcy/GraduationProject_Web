package com.zcy.tool;

import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;


public class QiniuCloudUtil {
    // 设置需要操作的账号的AK和SK
    private static final String ACCESS_KEY = "3oUOo4xvtBAGxxpzywEryYSSoJyPZyNV0BJUMxO0";
    private static final String SECRET_KEY = "6etXGtsZiWcWhaR7NjfwuQb2mhX8cUGFhPgpPqVi";
    // 要上传的空间
    private static final String BUCKET_NAME = "zcyplus";
    //获取图片上传URL路径
    private static final String DOMAIN = "http://img.zcy.plus";
    private static final Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);

    public static String getToken(){
        return auth.uploadToken(BUCKET_NAME);
    }

    /**
     * 上传图片
     * @param uploadBytes 图片字节流数据
     * @param fileName 文件名
     * @return 图片URL
     * @throws QiniuException
     */
    public static String uploadImage(byte[] uploadBytes, String fileName) throws QiniuException {
        Configuration cfg = new Configuration(Region.huanan());
        cfg.useHttpsDomains = false;
        UploadManager uploadManager = new UploadManager(cfg);

        String upToken = getToken();
        Response response = uploadManager.put(uploadBytes, fileName, upToken);
        return DOMAIN+"/"+fileName;
    }

    /**
     * 删除图片，若未出现异常则删除成功
     * @param src 路径和文件名
     * @throws QiniuException
     */
    public static void deleteImage(String src) throws QiniuException {
        Configuration cfg = new Configuration(Region.huanan());
        cfg.useHttpsDomains = false;

        BucketManager bucketManager = new BucketManager(auth, cfg);
        bucketManager.delete(BUCKET_NAME, src);
    }
}
