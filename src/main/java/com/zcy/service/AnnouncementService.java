package com.zcy.service;

import com.zcy.entity.Announcement;

import java.util.List;

public interface AnnouncementService {

    /**
     * 获取总数
     * @return
     */
    Integer getTotal();

    /**
     * 添加公告
     * @param announcement
     * @return
     */
    int add(Announcement announcement);

    /**
     * 删除公告
     * @param announcementId 公告ID
     * @return
     */
    int deleteByAnId(Integer announcementId);

    /**
     * 更新公告
     * @param announcement 公告ID
     * @return
     */
    int updateByAnId(Announcement announcement);

    /**
     * 更新公告
     * @param announcement 公告对象
     * @return
     */
    int updateByTypeId(Announcement announcement);

    /**
     * 获取所有公告
     * @return 公告列表
     */
    List<Announcement> getAll();
}
