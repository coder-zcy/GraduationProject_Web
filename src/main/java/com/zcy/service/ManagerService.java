package com.zcy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zcy.entity.Manager;
import com.zcy.entity.User;

import java.util.List;

/**
 * (Manager)表服务接口
 *
 * @author makejava
 * @since 2021-04-23 17:55:32
 */
public interface ManagerService extends IService<Manager> {

    /**
     * 获取总数
     * @return
     */
    Integer getTotal();

    /**
     * 通过指定条件获取管理员
     * @return 管理员列表
     */
    List<Manager> getManagerByCondition(Manager manager);

    /**
     * 获取指定姓名的用户
     * @return 管理员
     */
    Manager getManagerByName(String managerName);

    /**
     * 获取指定页面的所有管理员
     * @return 管理员列表
     */
    List<Manager> getManagerByPage(int current, int size);

    /**
     * 新增一名管理员
     * @param manager 管理员对象
     * @return 影响的行数
     */
    int addManager(Manager manager);

    /**
     * 通过ID更新管理员
     * @param manager 管理员对象
     * @return 影响的行数
     */
    int updateManagerById(Manager manager);

    /**
     * 通过ID删除管理员
     * @param managerId 管理员ID
     * @return 影响的行数
     */
    int deleteManagerById(int managerId);

    /**
     * 通过ID批量删除管理员
     * @param managersId 管理员的ID
     * @return 影响的行数
     */
    int deleteManagersById(List<Integer> managersId);
}
