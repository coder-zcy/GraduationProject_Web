package com.zcy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mysql.cj.util.StringUtils;
import com.zcy.entity.Comment;
import com.zcy.entity.Shop;

import java.util.List;

/**
 * (Comment)表服务接口
 *
 * @author makejava
 * @since 2021-04-23 17:55:31
 */
public interface CommentService extends IService<Comment> {

    /**
     * 获取总数
     * @return
     */
    Integer getTotal();

    /**
     * 获取用户1 和 用户2 的交流信息，其中 图片是保存userId2的头像
     * @param userId1 对方 ID
     * @param userId2 自己 ID
     * @return 评论列表
     */
    List<Comment> getCommentUTU(Integer userId1, Integer userId2);

    /**
     * 获取对指定商品的所有评论，评论图片是商品图
     * @param commodityId 商品ID
     * @return
     */
    List<Comment> getCommentUTC(Integer commodityId);

    /**
     * 通过指定条件获取评论
     * @return 评论列表
     */
    List<Comment> getCommentByCondition(Comment comment);

    /**
     * 将 对方 发送给自己的消息全部设置为已读
     * @param targetId
     * @param selfId
     * @return
     */
    int setCommentRead(Integer targetId, Integer selfId);

    /**
     * 获取指定店主ID的评论
     * @return 评论
     */
    Comment getCommentByUserId(Integer userId);

    /**
     * 获取指定页面的所有评论
     * @return 评论列表
     */
    List<Comment> getCommentByPage(int current, int size);

    /**
     * 新增一名评论
     * @param comment 评论对象
     * @return 影响的行数
     */
    int addComment(Comment comment);

    /**
     * 通过ID更新评论
     * @param comment 评论对象
     * @return 影响的行数
     */
    int updateCommentById(Comment comment);

    /**
     * 通过对象ID和类型，删除评论
     * @param id 对象ID
     * @param type 店铺ID
     * @return 影响的行数
     */
    int deleteCommentByTypeId(int id, int type);

    /**
     * 通过ID批量删除评论
     * @param commentsId 店铺的ID
     * @return 影响的行数
     */
    int deleteCommentsById(List<Integer> commentsId);

    /**
     * 通过用户ID删除评论
     * @param userId 用户ID
     * @return 影响的行数
     */
    int deleteCommentsByUserId(Integer userId);

}
