package com.zcy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zcy.entity.Posts;
import com.zcy.entity.Shop;

import java.util.List;

/**
 * (Posts)表服务接口
 *
 * @author makejava
 * @since 2021-04-23 17:55:32
 */
public interface PostsService extends IService<Posts> {

    /**
     * 获取总数
     * @return
     */
    Integer getTotal();

    /**
     * 根据条件获取贴子列表。可区间查询奖赏
     * @param posts 贴子对象
     * @return
     */
    List<Posts> getPostsByCondition(Posts posts);

    /**
     * 根据作者名获取帖子
     * @param authorName
     * @return
     */
    List<Posts> getPostsByAuthorName(String authorName);

    /**
     * 根据标题获取
     * @param title
     * @return
     */
    List<Posts> getPostsByTitle(String title);
    /**
     * 根据类型获取帖子
     * @param type
     * @return
     */
    List<Posts> getPostsByType(String type);

    /**
     * 获取指定页面的所有贴子
     * @return 贴子列表
     */
    List<Posts> getPostsByPage(int current, int size);

    /**
     * 新增一名贴子
     * @param posts 贴子对象
     * @return 影响的行数
     */
    int addPosts(Posts posts);

    /**
     * 通过ID更新贴子
     * @param posts 贴子对象
     * @return 影响的行数
     */
    int updatePostsById(Posts posts);

    /**
     * 通过ID批量删除贴子
     * @param postsId 贴子的ID
     * @return 影响的行数
     */
    int deletePostsById(List<Integer> postsId);

    /**
     * 通过用户ID删除贴子
     * @param userId 用户ID
     * @return 影响的行数
     */
    int deletePostsByAuthorId(Integer userId);
}
