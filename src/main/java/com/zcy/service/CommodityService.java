package com.zcy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zcy.entity.Commodity;
import com.zcy.entity.Shop;
import com.zcy.entity.User;

import java.util.List;

/**
 * (Commodity)表服务接口
 *
 * @author makejava
 * @since 2021-04-23 17:55:31
 */
public interface CommodityService extends IService<Commodity> {
    /**
     * 获取总数
     * @return
     */
    Integer getTotal();

    /**
     * 通过指定条件获取商品
     * @return 商品列表
     */
    List<Commodity> getCommodityByCondition(Commodity commodity);

    /**
     * 通过价格获取商品
     * @return 商品列表
     */
    List<Commodity> getCommodityByPrice(String priceMin, String priceMax);

    /**
     * 通过数量搜索
     * @param amountMin 最低
     * @param amountMax 最高
     * @return
     */
    List<Commodity> getCommodityByAmount(String amountMin, String amountMax);

    /**
     * 获取指定ID的商品
     * @return 商品
     */
    Commodity getCommodityById(Integer commodityId);

    /**
     * 根据店铺ID获取指定页面的所有商品
     * @return 商品列表
     */
    List<Commodity> getCommodityByPage(int current, int size, Integer currentShopId);

    /**
     * 新增一名商品
     * @param commodity 商品对象
     * @return 影响的行数
     */
    int addCommodity(Commodity commodity);

    /**
     * 通过ID更新商品
     * @param commodity 商品对象
     * @return 影响的行数
     */
    int updateCommodityById(Commodity commodity);

    /**
     * 通过ID删除商品
     * @param commodityId 商品ID
     * @return 影响的行数
     */
    int deleteCommodityById(int commodityId);

    /**
     * 通过ID批量删除商品
     * @param commoditiesId 商品的ID
     * @return 影响的行数
     */
    int deleteCommoditiesById(List<Integer> commoditiesId);

    /**
     * 通过店铺ID删除商品
     * @param shopId 商铺ID
     * @return 影响的行数
     */
    int deleteCommoditiesByShopId(Integer shopId);

    /**
     * 通过用户ID删除商品
     * @param userId 用户ID
     * @return 影响的行数
     */
    int deleteCommoditiesByUserId(Integer userId);
}
