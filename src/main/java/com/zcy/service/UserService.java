package com.zcy.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zcy.entity.User;
import com.zcy.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (User)表服务接口
 *
 * @author makejava
 * @since 2021-04-23 17:55:33
 */
public interface UserService extends IService<User> {

    /**
     * 获取总数
     * @return
     */
    Integer getTotal();

    /**
     * 用户登录
     * @param user 用户对象
     * @return
     */
    User userLogin(User user);

    /**
     * 通过指定条件获取用户
     * @return 用户列表
     */
    List<User> getUserByCondition(User user);

    /**
     * 获取指定姓名的用户
     * @return 用户
     */
    User getUserByName(String userName);

    /**
     * 获取指定ID的用户
     * @return 用户
     */
    User getUserById(Integer userId);

    /**
     * 获取指定页面的所有用户
     * @return 用户列表
     */
    List<User> getUserByPage(int current, int size);

    /**
     * 新增一名用户
     * @param user 用户对象
     * @return 影响的行数
     */
    int addUser(User user);

    /**
     * 通过ID更新用户
     * @param user 用户对象
     * @return 影响的行数
     */
    int updateUserById(User user);

    /**
     * 通过ID删除用户
     * @param userId 用户ID
     * @return 影响的行数
     */
    int deleteUserById(int userId);

    /**
     * 通过ID批量删除用户
     * @param usersId 用户的ID
     * @return 影响的行数
     */
    int deleteUsersById(List<Integer> usersId);
}
