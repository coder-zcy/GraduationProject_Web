package com.zcy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zcy.entity.Manager;
import com.zcy.entity.Shop;
import com.zcy.entity.User;
import io.swagger.models.auth.In;

import java.util.List;

/**
 * (Shop)表服务接口
 *
 * @author makejava
 * @since 2021-04-23 17:55:32
 */
public interface ShopService extends IService<Shop> {

    /**
     * 获取总数
     * @return
     */
    Integer getTotal();

    /**
     * 通过指定条件获取店铺
     * @return 店铺列表
     */
    List<Shop> getShopByCondition(Shop shop);

    /**
     * 获取指定姓名的店铺
     * @return 店铺
     */
    Shop getShopByName(String shopName);

    /**
     * 获取指定店主ID的店铺
     * @return 店铺
     */
    Shop getShopByUserId(Integer userId);

    /**
     * 获取指定ID的店铺
     * @param shopId 店铺ID
     * @return
     */
    Shop getShopById(Integer shopId);

    /**
     * 获取指定页面的所有店铺
     * @return 店铺列表
     */
    List<Shop> getShopByPage(int current, int size);

    /**
     * 新增一名店铺
     * @param shop 店铺对象
     * @return 影响的行数
     */
    int addShop(Shop shop);

    /**
     * 通过ID更新店铺
     * @param shop 店铺对象
     * @return 影响的行数
     */
    int updateShopById(Shop shop);

    /**
     * 通过ID删除店铺
     * @param shopId 店铺ID
     * @return 影响的行数
     */
    int deleteShopById(int shopId);

    /**
     * 通过ID批量删除店铺
     * @param shopsId 店铺的ID
     * @return 影响的行数
     */
    int deleteShopsById(List<Integer> shopsId);

    /**
     * 通过店主ID删除店铺
     * @param userId 店主对象
     * @return 影响的行数
     */
    int deleteShopsByUserId(Integer userId);

    int updateShop(Shop shop);
}
