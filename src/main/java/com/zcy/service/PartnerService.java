package com.zcy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zcy.entity.Comment;
import com.zcy.entity.Manager;
import com.zcy.entity.Partner;
import com.zcy.entity.Posts;
import io.swagger.models.auth.In;

import java.util.List;

/**
 * (Partner)表服务接口
 *
 * @author makejava
 * @since 2021-04-23 17:55:33
 */
public interface PartnerService extends IService<Partner> {

    /**
     * 获取总数
     * @return
     */
    Integer getTotal();

    Integer getShopIdByUserId(Integer userId);
    Integer getPartner1IdByUserId(Integer userId);
    Integer getPartner2IdByUserId(Integer userId);

    /**
     * 判断是否为合伙人
     * @return
     */
    boolean isPartner(Integer userId);

    /**
     * 通过指定条件获取评论
     * @return 关系列表
     */
    List<Partner> getPartnerByCondition(Partner partner);

    /**
     * 获取指定页面的所有关系
     * @return 关系列表
     */
    List<Partner> getPartnerByPage(int current, int size);

    /**
     * 根据ID获取关系
     * @param shopKeeperId 店主ID
     * @return
     */
    Partner getPartnerByKeeperId(Integer shopKeeperId);
    /**
     * 根据ID获取关系
     * @param partner1Id 伙伴1ID
     * @return
     */
    Partner getPartnerByPartner1Id(Integer partner1Id);

    /**
     * 判断是否有店铺
     * @param userId
     * @return
     */
    boolean hasShop(Integer userId);

    /**
     * 根据ID获取关系
     * @param partner2Id 伙伴2ID
     * @return
     */
    Partner getPartnerByPartner2Id(Integer partner2Id);

    /**
     * 根据ID获取关系
     * @param shopId 店铺ID
     * @return
     */
    List<Partner> getPartnerByShopId(Integer shopId);


    /**
     * 新增一条关系
     * @return 影响行数
     */
    int addPartner(Partner partner);

    /**
     * 删除一条关系
     * @param shopKeeperId 店主ID
     * @return 影响行数
     */
    int deletePartnerByKeepId(Integer shopKeeperId);

    /**
     * 删除一条关系
     * @param partner1Id 伙伴1ID
     * @return 影响行数
     */
    int deletePartnerByPartner1Id(Integer partner1Id);

    /**
     * 删除一条关系
     * @param partner2Id 伙伴2ID
     * @return 影响行数
     */
    int deletePartnerByPartner2Id(Integer partner2Id);


    /**
     * 更新一条关系
     * @param partner 对象ID
     * @return 影响行数
     */
    int updatePartnerByKeepId(Partner partner);

    /**
     * 断开伙伴1
     * @param partner1Id 对象ID
     * @return 影响行数
     */
    int closePartnerByPartner1Id(Integer partner1Id);

    /**
     * 断开伙伴2
     * @param partner2Id 对象ID
     * @return 影响行数
     */
    int closePartnerByPartner2Id(Integer partner2Id);
}
