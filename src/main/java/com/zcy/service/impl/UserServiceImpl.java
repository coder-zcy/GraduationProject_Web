package com.zcy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mysql.cj.util.StringUtils;
import com.zcy.dao.UserDao;
import com.zcy.entity.User;
import com.zcy.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * (User)表服务实现类
 *
 * @author 张城阳
 * @since 2021-04-23 17:55:33
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {
    @Resource
    private UserDao userDao;

    @Override
    public Integer getTotal() {
        return userDao.selectCount(null);
    }

    @Override
    public User userLogin(User user) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();

        if (user.getUserName() != null)
            wrapper.eq("user_name", user.getUserName());
        if (user.getPassword() != null)
            wrapper.eq("password", user.getPassword());
        if (user.getPhoneNumber() != null)
            wrapper.eq("phone_number", user.getPhoneNumber());

        return userDao.selectOne(wrapper);
    }


    /**
     * 根据条件获取用户
     * @return 用户列表
     */
    @Override
    public List<User> getUserByCondition(User user){
        QueryWrapper<User> wrapper = new QueryWrapper<>();

        if (user.getUserId() != null)
            wrapper.eq("user_id", user.getUserId());

        if (!StringUtils.isNullOrEmpty(user.getUserName()))
            wrapper.eq("user_name", user.getUserName());

        if (!StringUtils.isNullOrEmpty(user.getPassword()))
            wrapper.eq("password", user.getPassword());

        if (!StringUtils.isNullOrEmpty(user.getPhoneNumber()))
            wrapper.eq("phone_number", user.getPhoneNumber());

        //不设置头像查询

        if (user.getPoint() != null)
            wrapper.eq("point", user.getPoint());

        if (user.getSex() != null)
            wrapper.eq("sex", user.getSex());

        //不设置签名插叙

        //地址查询 xx省 xx市 xx区 xx县

        if (!StringUtils.isNullOrEmpty(user.getStudentId()))
            wrapper.eq("student_id", user.getStudentId());

        if (!StringUtils.isNullOrEmpty(user.getSchool()))
            wrapper.eq("school", user.getSchool());

        if (!StringUtils.isNullOrEmpty(user.getGrade()))
            wrapper.eq("grade", user.getGrade());

        if (user.getCreateTime() != null){
            //页面传进来Fri May 29 00:00:00 CST 2020格式的类似时间需要格式化一下
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String timeFormat = sdf.format(user.getCreateTime());
            //create_time:数据库中的创建时间，需格式为年月日的格式与传进来的时间进行匹配
            //条件构造器apply的查询方式解决日期和时间之间查询上的冲突
            wrapper.apply("date_format(create_time, '%Y-%m-%d') = {0}", timeFormat);
        }

        return userDao.selectList(wrapper);
    }

    /**
     * 获取指定姓名的用户
     * @return 用户
     */
    @Override
    public User getUserByName(String userName){
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("user_name", userName);
        return userDao.selectOne(wrapper);
    }

    @Override
    public User getUserById(Integer userId) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        return userDao.selectOne(wrapper);
    }

    /**
     * 获取指定页面的用户
     * @return 用户列表
     */
    @Override
    public List<User> getUserByPage(int current, int size){
        Page<User> page = new Page<>(current, size);
        userDao.selectPage(page, null);
        return page.getRecords();
    }


    /**
     * 新增一名用户
     * @param user 用户对象
     * @return 影响的行数
     */
    @Override
    public int addUser(User user){
        return userDao.insert(user);
    }

    /**
     * 通过ID更新用户
     * @param user 用户对象
     * @return 影响的行数
     */
    @Override
    public int updateUserById(User user){
        return userDao.updateById(user);
    }

    /**
     * 通过ID删除用户
     * @param userId 用户ID
     * @return 影响的行数
     */
    @Override
    public int deleteUserById(int userId){
        return userDao.deleteById(userId);
    }

    /**
     * 通过ID批量删除用户
     * @param usersId 用户的ID
     * @return 影响的行数
     */
    @Override
    public int deleteUsersById(List<Integer> usersId) {
        return userDao.deleteBatchIds(usersId);
    }

}
