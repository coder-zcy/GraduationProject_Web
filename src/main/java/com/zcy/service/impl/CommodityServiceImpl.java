package com.zcy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mysql.cj.util.StringUtils;
import com.zcy.dao.CommodityDao;
import com.zcy.dao.ShopDao;
import com.zcy.entity.Commodity;
import com.zcy.entity.Shop;
import com.zcy.entity.User;
import com.zcy.service.CommodityService;
import com.zcy.service.ShopService;
import com.zcy.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * (Commodity)表服务实现类
 *
 * @author makejava
 * @since 2021-04-23 17:55:31
 */
@Service("commodityService")
public class CommodityServiceImpl extends ServiceImpl<CommodityDao, Commodity> implements CommodityService {
    @Resource
    CommodityDao commodityDao;

    @Resource
    ShopService shopService;

    @Resource
    UserService userService;


    @Override
    public Integer getTotal() {
        return commodityDao.selectCount(null);
    }

    /**
     * 根据条件获取商品
     * @return 商品列表
     */
    @Override
    public List<Commodity> getCommodityByCondition(Commodity commodity) {
        QueryWrapper<Commodity> wrapper = new QueryWrapper<>();

        if (commodity.getCmodId() != null)
            wrapper.eq("cmod_id", commodity.getCmodId());
        if (commodity.getShopId() != null)
            wrapper.eq("shop_id", commodity.getShopId());
        if (!StringUtils.isNullOrEmpty(commodity.getCmodName()))
            wrapper.like("cmod_name", commodity.getCmodName()+"%");
        if (commodity.getAmount() != null)
            wrapper.eq("amount", commodity.getAmount());
        if (!StringUtils.isNullOrEmpty(commodity.getType()))
            wrapper.eq("type", commodity.getType());
        if (commodity.getPrice() != null)
            wrapper.eq("price", commodity.getPrice());

        if (commodity.getCreateTime() != null){
            //页面传进来Fri May 29 00:00:00 CST 2020格式的类似时间需要格式化一下
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String timeFormat = sdf.format(commodity.getCreateTime());
            //create_time:数据库中的创建时间，需格式为年月日的格式与传进来的时间进行匹配
            //条件构造器apply的查询方式解决日期和时间之间查询上的冲突
            wrapper.apply("date_format(create_time, '%Y-%m-%d') = {0}", timeFormat);
        }
        if (commodity.getUpdateTime() != null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String timeFormat = sdf.format(commodity.getUpdateTime());
            wrapper.apply("date_format(update_time, '%Y-%m-%d') = {0}", timeFormat);
        }

        List<Commodity> list = commodityDao.selectList(wrapper);

        //将对应的店主和店铺添加到商品里，方便Android端查看
        List<Commodity> result = new ArrayList<>();
        for (Commodity commodity1 : list) {
            //查询该商品的所属店铺和店主
            Shop shop = shopService.getShopById(commodity1.getShopId());
            commodity1.setShopName(shop.getShopName());
            commodity1.setShopId(shop.getShopId());

            //查询该店主名字
            User user = userService.getUserById(shop.getUserId());
            commodity1.setUserName(user.getUserName());
            commodity1.setUserId(user.getUserId());

            result.add(commodity1);
        }

        return result;
    }

    /**
     * 通过价格搜索
     * @param priceMin 最低
     * @param priceMax 最高
     * @return
     */
    @Override
    public List<Commodity> getCommodityByPrice(String priceMin, String priceMax) {
        QueryWrapper<Commodity> wrapper = new QueryWrapper<>();
        wrapper.between("price", Double.parseDouble(priceMin), Double.parseDouble(priceMax));
        return commodityDao.selectList(wrapper);
    }

    /**
     * 通过数量搜索
     * @param amountMin 最低
     * @param amountMax 最高
     * @return
     */
    @Override
    public List<Commodity> getCommodityByAmount(String amountMin, String amountMax) {
        QueryWrapper<Commodity> wrapper = new QueryWrapper<>();
        wrapper.between("amount", Double.parseDouble(amountMin), Double.parseDouble(amountMax));
        return commodityDao.selectList(wrapper);
    }

    @Override
    public Commodity getCommodityById(Integer commodityId) {
        QueryWrapper<Commodity> wrapper = new QueryWrapper<>();
        wrapper.eq("cmod_id", commodityId);
        return commodityDao.selectOne(wrapper);
    }


    /**
     * 根据店铺ID获取指定页面的商品
     * @return 商品列表
     */
    @Override
    public List<Commodity> getCommodityByPage(int current, int size, Integer currentShopId) {
        QueryWrapper<Commodity> wrapper = new QueryWrapper<>();
        wrapper.eq("shop_id", currentShopId);
        Page<Commodity> page = new Page<>(current, size);
        commodityDao.selectPage(page, wrapper);
        return page.getRecords();
    }

    /**
     * 新增一件商品
     * @param commodity 商品对象
     * @return 影响的行数
     */
    @Override
    public int addCommodity(Commodity commodity) {
        return commodityDao.insert(commodity);
    }

    /**
     * 通过ID更新店铺
     * @param commodity 店铺对象
     * @return 影响的行数
     */
    @Override
    public int updateCommodityById(Commodity commodity) {
        return commodityDao.updateById(commodity);
    }

    /**
     * 通过ID删除商品
     * @param commodityId 商品ID
     * @return 影响的行数
     */
    @Override
    public int deleteCommodityById(int commodityId) {
        return commodityDao.deleteById(commodityId);
    }

    /**
     * 通过ID批量删除商品
     * @param commoditiesId 商品的ID
     * @return 影响的行数
     */
    @Override
    public int deleteCommoditiesById(List<Integer> commoditiesId) {
        return commodityDao.deleteBatchIds(commoditiesId);
    }

    /**
     * 通过商铺ID删除商品
     * @param shopId 店铺ID
     * @return 影响的行数
     */
    @Override
    public int deleteCommoditiesByShopId(Integer shopId) {
        QueryWrapper<Commodity> wrapper = new QueryWrapper<>();
        wrapper.eq("shop_id", shopId);
        return commodityDao.delete(wrapper);
    }

    /**
     * 通过用户ID删除商品
     * @param userId 商品ID
     * @return 影响的行数
     */
    @Override
    public int deleteCommoditiesByUserId(Integer userId) {

        Shop shop = shopService.getShopByUserId(userId);
        if (shop != null)
            return deleteCommoditiesByShopId(shop.getShopId());
        return 0;
    }
}
