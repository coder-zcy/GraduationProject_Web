package com.zcy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mysql.cj.util.StringUtils;
import com.zcy.dao.PostsDao;
import com.zcy.dao.UserDao;
import com.zcy.entity.Manager;
import com.zcy.entity.Posts;
import com.zcy.entity.Shop;
import com.zcy.entity.User;
import com.zcy.service.PostsService;
import com.zcy.service.UserService;
import io.swagger.models.auth.In;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * (Posts)表服务实现类
 *
 * @author makejava
 * @since 2021-04-23 17:55:32
 */
@Service("postsService")
public class PostsServiceImpl extends ServiceImpl<PostsDao, Posts> implements PostsService {

    @Resource
    private PostsDao postsDao;
    @Resource
    private UserService userService;

    @Override
    public Integer getTotal() {
        return postsDao.selectCount(null);
    }

    /**
     * 根据条件获取贴子列表。可区间查询奖赏
     * @param posts 贴子对象
     * @return
     */
    @Override
    public List<Posts> getPostsByCondition(Posts posts) {
        QueryWrapper<Posts> wrapper = new QueryWrapper<>();

        if (posts.getPostId() != null)
            wrapper.eq("post_id", posts.getPostId());
        if (posts.getReward() != null)
            wrapper.eq("reward", posts.getReward());
        //模糊查询主题
        if (!StringUtils.isNullOrEmpty(posts.getPostTitle()))
            wrapper.like("post_title", "%"+posts.getPostTitle()+"%");


        if (posts.getCreateTime() != null){
            //页面传进来Fri May 29 00:00:00 CST 2020格式的类似时间需要格式化一下
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String timeFormat = sdf.format(posts.getCreateTime());
            //create_time:数据库中的创建时间，需格式为年月日的格式与传进来的时间进行匹配
            //条件构造器apply的查询方式解决日期和时间之间查询上的冲突
            wrapper.apply("date_format(create_time, '%Y-%m-%d') = {0}", timeFormat);
        }
        if (posts.getUpdateTime() != null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String timeFormat = sdf.format(posts.getUpdateTime());
            wrapper.apply("date_format(update_time, '%Y-%m-%d') = {0}", timeFormat);
        }

        List<Posts> postsList = postsDao.selectList(wrapper);
        List<Posts> result = new ArrayList<>();
        for (Posts posts1 : postsList) {
            User user = userService.getUserById(posts1.getAuthorId());
            posts1.setAuthorName(user.getUserName());
            result.add(posts1);
        }
        return result;
    }

    /**
     * 根据作者名获取帖子
     * @param authorName
     * @return
     */
    @Override
    public List<Posts> getPostsByAuthorName(String authorName) {
        QueryWrapper<Posts> wrapper = new QueryWrapper<>();
        Integer authorId = userService.getUserByName(authorName).getUserId();
        wrapper.eq("authorId", authorId);

        List<Posts> postsList = postsDao.selectList(wrapper);
        List<Posts> result = new ArrayList<>();
        for (Posts posts1 : postsList) {
            User user = userService.getUserById(posts1.getAuthorId());
            posts1.setAuthorName(user.getUserName());
            result.add(posts1);
        }
        return result;
    }

    @Override
    public List<Posts> getPostsByTitle(String title) {
        QueryWrapper<Posts> wrapper = new QueryWrapper<>();
        wrapper.eq("post_title", title);

        List<Posts> postsList = postsDao.selectList(wrapper);
        List<Posts> result = new ArrayList<>();
        for (Posts posts1 : postsList) {
            User user = userService.getUserById(posts1.getAuthorId());
            posts1.setAuthorName(user.getUserName());
            result.add(posts1);
        }
        return result;
    }


    /**
     * 根据帖子类型获取
     * @param type
     * @return
     */
    @Override
    public List<Posts> getPostsByType(String type) {
        QueryWrapper<Posts> wrapper = new QueryWrapper<>();
        if (type.equals("reward"))
            wrapper.gt("reward", 0);
        else
            wrapper.eq("reward", 0);
        List<Posts> postsList = postsDao.selectList(wrapper);
        List<Posts> result = new ArrayList<>();
        for (Posts posts1 : postsList) {
            User user = userService.getUserById(posts1.getAuthorId());
            posts1.setAuthorName(user.getUserName());
            result.add(posts1);
        }
        return result;
    }

    /**
     * 获取指定页面的贴子
     * @return 贴子列表
     */
    @Override
    public List<Posts> getPostsByPage(int current, int size) {
        Page<Posts> page = new Page<>(current, size);
        postsDao.selectPage(page, null);
        return page.getRecords();
    }

    /**
     * 新增一个贴子
     * @param posts 贴子对象
     * @return 影响的行数
     */
    @Override
    public int addPosts(Posts posts) {
        return postsDao.insert(posts);
    }

    /**
     * 通过ID更新贴子
     * @param posts 贴子对象
     * @return 影响的行数
     */
    @Override
    public int updatePostsById(Posts posts) {
        return postsDao.updateById(posts);
    }

    /**
     * 通过ID删除贴子
     * @param postsId 管理员ID
     * @return 影响的行数
     */
    @Override
    public int deletePostsById(List<Integer> postsId) {
        return postsDao.deleteBatchIds(postsId);
    }

    /**
     * 通过用户ID删除贴子
     * @param userId 用户ID
     * @return
     */
    @Override
    public int deletePostsByAuthorId(Integer userId) {
        QueryWrapper<Posts> wrapper = new QueryWrapper<>();
        wrapper.eq("author_id", userId);
        return postsDao.delete(wrapper);
    }
}
