package com.zcy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zcy.dao.AnnouncementDao;
import com.zcy.entity.Announcement;
import com.zcy.service.AnnouncementService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Announcement)表服务实现类
 *
 * @author makejava
 * @since 2021-04-23 17:55:31
 */
@Service("announcementService")
public class AnnouncementServiceImpl implements AnnouncementService {

    @Resource
    private AnnouncementDao announcementDao;

    @Override
    public Integer getTotal() {
        return announcementDao.selectCount(null);
    }

    @Override
    public int add(Announcement announcement) {
        return announcementDao.insert(announcement);
    }

    @Override
    public int deleteByAnId(Integer announcementId) {
        return deleteByAnId(announcementId);
    }

    @Override
    public int updateByAnId(Announcement announcement) {
        return announcementDao.updateById(announcement);
    }

    @Override
    public int updateByTypeId(Announcement announcement) {
        QueryWrapper<Announcement> wrapper = new QueryWrapper<>();

        wrapper.eq("type", announcement.getType());
        wrapper.eq("id", announcement.getId());

        return announcementDao.update(announcement, wrapper);
    }

    @Override
    public List<Announcement> getAll() {
        return announcementDao.selectList(null);
    }
}
