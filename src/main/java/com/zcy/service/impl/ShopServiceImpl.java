package com.zcy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mysql.cj.util.StringUtils;
import com.zcy.dao.ManagerDao;
import com.zcy.dao.ShopDao;
import com.zcy.entity.Manager;
import com.zcy.entity.Partner;
import com.zcy.entity.Shop;
import com.zcy.entity.User;
import com.zcy.service.PartnerService;
import com.zcy.service.ShopService;
import com.zcy.service.UserService;
import io.swagger.models.auth.In;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * (Shop)表服务实现类
 *
 * @author makejava
 * @since 2021-04-23 17:55:33
 */
@Service("shopService")
public class ShopServiceImpl extends ServiceImpl<ShopDao, Shop> implements ShopService {

    @Resource
    private ShopDao shopDao;
    @Resource
    private UserService userService;
    @Resource
    private PartnerService partnerService;

    @Override
    public Integer getTotal() {
        return shopDao.selectCount(null);
    }

    /**
     * 根据条件获取店铺
     * @return 店铺列表
     */
    @Override
    public List<Shop> getShopByCondition(Shop shop) {
        QueryWrapper<Shop> wrapper = new QueryWrapper<>();

        if (shop.getShopId() != null)
            wrapper.eq("shop_id", shop.getShopId());
        if (shop.getUserId() != null)
            wrapper.eq("user_id", shop.getUserId());
        if (!StringUtils.isNullOrEmpty(shop.getShopName()))
            wrapper.eq("shop_name", shop.getShopName());
        //模糊搜索权限关键字
        if (!StringUtils.isNullOrEmpty(shop.getKeywords())){
            String[] keywords = shop.getKeywords().split("\\|");
            for (int i = 0; i < keywords.length; i++){
                wrapper.like("keywords", "%"+keywords[i]+"%");
            }
        }
        if (shop.getCreateTime() != null){
            //页面传进来Fri May 29 00:00:00 CST 2020格式的类似时间需要格式化一下
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String timeFormat = sdf.format(shop.getCreateTime());
            //create_time:数据库中的创建时间，需格式为年月日的格式与传进来的时间进行匹配
            //条件构造器apply的查询方式解决日期和时间之间查询上的冲突
            wrapper.apply("date_format(create_time, '%Y-%m-%d') = {0}", timeFormat);
        }
        if (shop.getUpdateTime() != null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String timeFormat = sdf.format(shop.getUpdateTime());
            wrapper.apply("date_format(update_time, '%Y-%m-%d') = {0}", timeFormat);
        }

        return shopDao.selectList(wrapper);
    }

    /**
     * 获取指定姓名的店铺
     * @return 店铺
     */
    @Override
    public Shop getShopByName(String shopName) {
        QueryWrapper<Shop> wrapper = new QueryWrapper<>();
        wrapper.eq("shop_name", shopName);
        return shopDao.selectOne(wrapper);
    }

    /**
     * 获取指定姓名的店铺
     * @return 店铺
     */
    @Override
    public Shop getShopByUserId(Integer userId) {
        QueryWrapper<Shop> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        return shopDao.selectOne(wrapper);
    }

    @Override
    public Shop getShopById(Integer shopId) {
        QueryWrapper<Shop> wrapper = new QueryWrapper<>();
        wrapper.eq("shop_id", shopId);
        return shopDao.selectOne(wrapper);
    }

    /**
     * 获取指定页面的店铺
     * @return 店铺列表
     */
    @Override
    public List<Shop> getShopByPage(int current, int size) {
        Page<Shop> page = new Page<>(current, size);
        shopDao.selectPage(page, null);
        return page.getRecords();
    }

    /**
     * 新增一名店铺
     * @param shop 店铺对象
     * @return 影响的行数
     */
    @Override
    public int addShop(Shop shop) {
        return shopDao.insert(shop);
    }

    /**
     * 通过ID更新店铺
     * @param shop 店铺对象
     * @return 影响的行数
     */
    @Override
    public int updateShopById(Shop shop) {
        return shopDao.updateById(shop);
    }

    /**
     * 通过ID删除店铺
     * @param shopId 店铺ID
     * @return 影响的行数
     */
    @Override
    public int deleteShopById(int shopId) {
        return shopDao.deleteById(shopId);
    }

    /**
     * 通过ID批量删除店铺
     * @param shopsId 店铺的ID
     * @return 影响的行数
     */
    @Override
    public int deleteShopsById(List<Integer> shopsId) {
        return shopDao.deleteBatchIds(shopsId);
    }

    /**
     * 通过店主ID删除店铺
     * @param userId 店主ID
     * @return 影响的行数
     */
    @Override
    public int deleteShopsByUserId(Integer userId) {
        QueryWrapper<Shop> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        return shopDao.delete(wrapper);
    }

    @Override
    public int updateShop(Shop shop) {
        QueryWrapper<Shop> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", shop.getUserId());
        shop.setUserId(null);
        return shopDao.update(shop, wrapper);
    }
}
