package com.zcy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mysql.cj.util.StringUtils;
import com.zcy.dao.PartnerDao;
import com.zcy.entity.Comment;
import com.zcy.entity.Manager;
import com.zcy.entity.Partner;
import com.zcy.service.PartnerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.Part;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * (Partner)表服务实现类
 *
 * @author makejava
 * @since 2021-04-23 17:55:33
 */
@Service("userShopService")
public class PartnerServiceImpl extends ServiceImpl<PartnerDao, Partner> implements PartnerService {

    @Resource
    private PartnerDao partnerDao;

    @Override
    public Integer getTotal() {
        return partnerDao.selectCount(null);
    }

    @Override
    public Integer getShopIdByUserId(Integer userId) {
        QueryWrapper<Partner> wrapper = new QueryWrapper<>();
        wrapper.eq("shopkeeper_id", userId)
                .or().eq("partner1_id" , userId)
                .or().eq("partner2_id", userId);
        Partner partner = partnerDao.selectOne(wrapper);
        if (partner != null) return partner.getShopId();
        else return null;
    }

    @Override
    public Integer getPartner1IdByUserId(Integer userId) {
        QueryWrapper<Partner> wrapper = new QueryWrapper<>();
        wrapper.eq("shopkeeper_id", userId);
        Partner partner = partnerDao.selectOne(wrapper);
        if (partner != null)
            return partner.getPartner1Id();
        else
            return null;
    }

    @Override
    public Integer getPartner2IdByUserId(Integer userId) {
        QueryWrapper<Partner> wrapper = new QueryWrapper<>();
        wrapper.eq("shopkeeper_id", userId);
        Partner partner = partnerDao.selectOne(wrapper);
        if (partner != null)
            return partner.getPartner2Id();
        else
            return null;
    }

    @Override
    public boolean isPartner(Integer userId) {
        QueryWrapper<Partner> wrapper = new QueryWrapper<>();
        wrapper.eq("partner1_id", userId).or().eq("partner2_id", userId);
        if (partnerDao.selectCount(wrapper) > 0)
            return true;
        return false;
    }

    @Override
    public List<Partner> getPartnerByCondition(Partner partner) {
        QueryWrapper<Partner> wrapper = new QueryWrapper<>();

        if (partner.getShopkeeperId() != null)
            wrapper.eq("shopkeeper_id", partner.getShopkeeperId());
        if (partner.getPartner1Id() != null)
            wrapper.eq("partner1_id", partner.getPartner1Id());
        if (partner.getPartner2Id() != null)
            wrapper.eq("partner2_id", partner.getPartner2Id());
        if (partner.getShopId() != null)
            wrapper.eq("shop_id", partner.getShopId());


        if (partner.getCreateTime() != null){
            //页面传进来Fri May 29 00:00:00 CST 2020格式的类似时间需要格式化一下
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String timeFormat = sdf.format(partner.getCreateTime());
            //create_time:数据库中的创建时间，需格式为年月日的格式与传进来的时间进行匹配
            //条件构造器apply的查询方式解决日期和时间之间查询上的冲突
            wrapper.apply("date_format(create_time, '%Y-%m-%d') = {0}", timeFormat);
        }
        if (partner.getUpdateTime() != null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String timeFormat = sdf.format(partner.getUpdateTime());
            wrapper.apply("date_format(update_time, '%Y-%m-%d') = {0}", timeFormat);
        }
        return partnerDao.selectList(wrapper);
    }

    @Override
    public List<Partner> getPartnerByPage(int current, int size) {
        Page<Partner> page = new Page<>(current, size);
        partnerDao.selectPage(page, null);
        return page.getRecords();
    }


    @Override
    public Partner getPartnerByKeeperId(Integer shopKeeperId) {
        QueryWrapper<Partner> wrapper = new QueryWrapper<>();
        wrapper.eq("shopkeeper_id", shopKeeperId);
        return partnerDao.selectOne(wrapper);
    }

    @Override
    public Partner getPartnerByPartner1Id(Integer partner1Id) {
        QueryWrapper<Partner> wrapper = new QueryWrapper<>();
        wrapper.eq("partner1_id", partner1Id);
        return partnerDao.selectOne(wrapper);
    }

    @Override
    public boolean hasShop(Integer userId) {
        QueryWrapper<Partner> wrapper = new QueryWrapper<>();
        wrapper.eq("shopkeeper_id", userId)
                .or().eq("partner1_id" , userId)
                .or().eq("partner2_id", userId);
        if (partnerDao.selectCount(wrapper) > 0)
            return true;
        return false;
    }

    @Override
    public Partner getPartnerByPartner2Id(Integer partner2Id) {
        QueryWrapper<Partner> wrapper = new QueryWrapper<>();
        wrapper.eq("partner2_id", partner2Id);
        return partnerDao.selectOne(wrapper);
    }

    @Override
    public List<Partner> getPartnerByShopId(Integer shopId) {
        QueryWrapper<Partner> wrapper = new QueryWrapper<>();
        wrapper.eq("shop_id", shopId);
        return partnerDao.selectList(wrapper);
    }

    @Override
    public int addPartner(Partner partner) {
        return partnerDao.insert(partner);
    }

    @Override
    public int deletePartnerByKeepId(Integer shopKeeperId) {
        QueryWrapper<Partner> wrapper = new QueryWrapper<>();
        wrapper.eq("shopkeeper_id", shopKeeperId);
        return partnerDao.delete(wrapper);
    }

    @Override
    public int deletePartnerByPartner1Id(Integer partner1Id) {
        QueryWrapper<Partner> wrapper = new QueryWrapper<>();
        wrapper.eq("partner1_id", partner1Id);
        return partnerDao.delete(wrapper);
    }

    @Override
    public int deletePartnerByPartner2Id(Integer partner2Id) {
        QueryWrapper<Partner> wrapper = new QueryWrapper<>();
        wrapper.eq("partner2_id", partner2Id);
        return partnerDao.delete(wrapper);
    }

    @Override
    public int updatePartnerByKeepId(Partner partner) {
        return partnerDao.updateById(partner);
    }

    @Override
    public int closePartnerByPartner1Id(Integer partner1Id) {
        Partner partner = getPartnerByPartner1Id(partner1Id);
        partner.setPartner1Id(null);
        return partnerDao.updateById(partner);
    }

    @Override
    public int closePartnerByPartner2Id(Integer partner2Id) {
        Partner partner = getPartnerByPartner1Id(partner2Id);
        partner.setPartner1Id(null);
        return partnerDao.updateById(partner);
    }
}
