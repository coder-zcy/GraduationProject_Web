package com.zcy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mysql.cj.util.StringUtils;
import com.zcy.dao.ManagerDao;
import com.zcy.entity.Manager;
import com.zcy.service.ManagerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * (Manager)表服务实现类
 *
 * @author makejava
 * @since 2021-04-23 17:55:32
 */
@Service("managerService")
public class ManagerServiceImpl extends ServiceImpl<ManagerDao, Manager> implements ManagerService {

    @Resource
    private ManagerDao managerDao;

    @Override
    public Integer getTotal() {
        return managerDao.selectCount(null);
    }

    /**
     * 根据条件获取管理员
     *
     * @return 管理员列表
     */
    @Override
    public List<Manager> getManagerByCondition(Manager manager) {
        QueryWrapper<Manager> wrapper = new QueryWrapper<>();

        if (manager.getManagerId() != null)
            wrapper.eq("manager_id", manager.getManagerId());
        if (!StringUtils.isNullOrEmpty(manager.getManagerName()))
            wrapper.eq("manager_name", manager.getManagerName());
        if (!StringUtils.isNullOrEmpty(manager.getPassword()))
            wrapper.eq("password", manager.getPassword());
        //模糊搜索权限关键字
        if (!StringUtils.isNullOrEmpty(manager.getAuthority())) {
            String[] authority = manager.getAuthority().split("\\|");
            for (int i = 0; i < authority.length; i++) {
                wrapper.like("authority", "%" + authority[i] + "%");
            }
        }
        if (manager.getCreateTime() != null) {
            //页面传进来Fri May 29 00:00:00 CST 2020格式的类似时间需要格式化一下
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String timeFormat = sdf.format(manager.getCreateTime());
            //create_time:数据库中的创建时间，需格式为年月日的格式与传进来的时间进行匹配
            //条件构造器apply的查询方式解决日期和时间之间查询上的冲突
            wrapper.apply("date_format(create_time, '%Y-%m-%d') = {0}", timeFormat);
        }
        if (manager.getUpdateTime() != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String timeFormat = sdf.format(manager.getUpdateTime());
            wrapper.apply("date_format(update_time, '%Y-%m-%d') = {0}", timeFormat);
        }

        return managerDao.selectList(wrapper);
    }

    /**
     * 获取指定姓名的管理员
     *
     * @return 管理员
     */
    @Override
    public Manager getManagerByName(String managerName) {
        QueryWrapper<Manager> wrapper = new QueryWrapper<>();
        wrapper.eq("manager_name", managerName);
        return managerDao.selectOne(wrapper);
    }

    /**
     * 获取指定页面的管理员
     *
     * @return 管理员列表
     */
    @Override
    public List<Manager> getManagerByPage(int current, int size) {
        Page<Manager> page = new Page<>(current, size);
        managerDao.selectPage(page, null);
        return page.getRecords();
    }


    /**
     * 新增一名管理员
     *
     * @param manager 管理员对象
     * @return 影响的行数
     */
    @Override
    public int addManager(Manager manager) {
        return managerDao.insert(manager);
    }

    /**
     * 通过ID更新管理员
     *
     * @param manager 管理员对象
     * @return 影响的行数
     */
    @Override
    public int updateManagerById(Manager manager) {
        return managerDao.updateById(manager);
    }

    /**
     * 通过ID删除管理员
     *
     * @param managerId 管理员ID
     * @return 影响的行数
     */
    @Override
    public int deleteManagerById(int managerId) {
        return managerDao.deleteById(managerId);
    }

    /**
     * 通过ID批量删除管理员
     *
     * @param managersId 管理员的ID
     * @return 影响的行数
     */
    @Override
    public int deleteManagersById(List<Integer> managersId) {
        return managerDao.deleteBatchIds(managersId);
    }

}
