package com.zcy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mysql.cj.util.StringUtils;
import com.zcy.dao.CommentDao;
import com.zcy.dao.UserDao;
import com.zcy.entity.Comment;
import com.zcy.entity.Commodity;
import com.zcy.entity.Shop;
import com.zcy.entity.User;
import com.zcy.service.CommentService;
import com.zcy.service.CommodityService;
import com.zcy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

/**
 * (Comment)表服务实现类
 *
 * @author makejava
 * @since 2021-04-23 17:55:31
 */
@Service("commentService")
public class CommentServiceImpl extends ServiceImpl<CommentDao, Comment> implements CommentService {

    @Resource
    CommentDao commentDao;
    @Resource
    UserService userService;
    @Resource
    CommodityService commodityService;

    @Override
    public Integer getTotal() {
        return commentDao.selectCount(null);
    }

    @Override
    public List<Comment> getCommentUTU(Integer userId1, Integer userId2) {
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();

        wrapper.and(wrapper1->wrapper1.eq("user_id", userId1).eq("id",userId2));
        wrapper.or(b->b.eq("id", userId1).eq("user_id",userId2));
        wrapper.eq("type", 0);//针对用户交流类型
        wrapper.orderByAsc("create_time");
        //修改评论的图片为自己的的头像
        List<Comment> comments = commentDao.selectList(wrapper);
        if (comments == null)
            return null;

        List<Comment> result = new ArrayList<>();

        User user1 = userService.getUserById(userId1);
        User user2 = userService.getUserById(userId2);
        for (Comment comment : comments) {
            if (comment.getUserId().equals(userId1))
                comment.setCmenImg(user1.getAvatar());
            else if (comment.getUserId().equals(userId2))
                comment.setCmenImg(user2.getAvatar());
            result.add(comment);
        }
        return result;
    }

    @Override
    public List<Comment> getCommentUTC(Integer commodityId) {
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        wrapper.eq("type", 2);//商品类型评论
        wrapper.eq("id", commodityId);//指定商品ID

        List<Comment> comments = commentDao.selectList(wrapper);
        if (comments == null)
            return null;

        List<Comment> result = new ArrayList<>();
        Commodity commodity = commodityService.getCommodityById(commodityId);
        for (Comment comment : comments) {
            comment.setCmenImg(commodity.getCmodImg());
            result.add(comment);
        }
        return commentDao.selectList(wrapper);
    }

    @Override
    public List<Comment> getCommentByCondition(Comment comment) {
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();

        if (comment.getCmenId() != null)
            wrapper.eq("cmen_id", comment.getCmenId());
        if (comment.getUserId() != null)
            wrapper.eq("user_id", comment.getUserId());
        if (!StringUtils.isNullOrEmpty(comment.getCmenContent()))
            wrapper.like("cmen_content", "%"+comment.getCmenContent()+"%");
        //按类型搜索
        if (comment.getType() != null)
            wrapper.eq("type", comment.getType());
        if (comment.getId() != null)
            wrapper.eq("id", comment.getId());

        if (comment.getCreateTime() != null){
            //页面传进来Fri May 29 00:00:00 CST 2020格式的类似时间需要格式化一下
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String timeFormat = sdf.format(comment.getCreateTime());
            //create_time:数据库中的创建时间，需格式为年月日的格式与传进来的时间进行匹配
            //条件构造器apply的查询方式解决日期和时间之间查询上的冲突
            wrapper.apply("date_format(create_time, '%Y-%m-%d') = {0}", timeFormat);
        }
        if (comment.getUpdateTime() != null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String timeFormat = sdf.format(comment.getUpdateTime());
            wrapper.apply("date_format(update_time, '%Y-%m-%d') = {0}", timeFormat);
        }
        return commentDao.selectList(wrapper);
    }

    @Override
    public int setCommentRead(Integer targetId, Integer selfId) {
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        wrapper.eq("userId", targetId);
        wrapper.eq("id", selfId);

        Comment comment = new Comment();
        comment.setIsRead(1);

        return commentDao.update(comment, wrapper);
    }

    @Override
    public Comment getCommentByUserId(Integer userId) {
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        return commentDao.selectOne(wrapper);
    }

    @Override
    public List<Comment> getCommentByPage(int current, int size) {
        Page<Comment> page = new Page<>(current, size);
        commentDao.selectPage(page, null);
        return page.getRecords();
    }

    @Override
    public int addComment(Comment comment) {
        return commentDao.insert(comment);
    }

    @Override
    public int updateCommentById(Comment comment) {
        return commentDao.updateById(comment);
    }

    @Override
    public int deleteCommentByTypeId(int id, int type) {
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        wrapper.eq("type", type);
        wrapper.eq("id", id);
        return commentDao.delete(wrapper);
    }

    @Override
    public int deleteCommentsById(List<Integer> commentsId) {
        return commentDao.deleteBatchIds(commentsId);
    }

    @Override
    public int deleteCommentsByUserId(Integer userId) {
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        return commentDao.delete(wrapper);
    }
}
