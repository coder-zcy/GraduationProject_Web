package com.zcy.config;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.mgt.RememberMeManager;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {

    /**
     * 记住我功能-设置cookie对象;
     * 该方法方法是设置Cookie的生成模版，比如cookie的name，cookie的有效时间等等。
     */
    @Bean
    public SimpleCookie rememberMeCookie(){
        //这个参数是cookie的名称，对应前端的checkbox的name = rememberMe
        SimpleCookie simpleCookie = new SimpleCookie("rememberMe");
        //<!-- 记住我cookie生效时间10天 ,单位秒;-->
        simpleCookie.setMaxAge(3*24*60*69);
        return simpleCookie;
    }
    /**
     * 记住我功能-设置cookie管理对象;
     * 该方法生成rememberMe管理器，而且要将这个rememberMe管理器设置到securityManager中
     * @return
     */
    @Bean
    public CookieRememberMeManager rememberMeManager(){
        CookieRememberMeManager cookieRememberMeManager = new CookieRememberMeManager();
        cookieRememberMeManager.setCookie(rememberMeCookie());
        //rememberMe cookie加密的密钥 建议每个项目都不一样 默认AES算法 密钥长度(128 256 512 位)
        cookieRememberMeManager.setCipherKey(Base64.decode("4AvVhmFLUs0KTA3Kprsdag=="));
        return cookieRememberMeManager;
    }

    //3. ShiroFilterFactoryBean，后续的操作在这里进行添加，例如登录拦截
    @Bean
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(@Qualifier("securityManager") DefaultWebSecurityManager securityManager){
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        //设置安全管理器
        bean.setSecurityManager(securityManager);

        /** 添加Shiro的内置过滤器，进行登录拦截。
         *  anon：无需认证就能访问
         *  authc：必须认证才能访问
         *  user：必须拥有"记住我"功能才能访问，一般不用
         *  perms：拥有"对某个资源的权限"才能访问
         *  role：拥有某个角色权限才能访问
         */
        Map<String, String> filterMap = new LinkedHashMap<>();

        //设置index页面为需要相应权限才能访问
        filterMap.put("/login/**", "anon");//登录请求不拦截
        filterMap.put("/js/**", "anon");//静态资源不拦截
        filterMap.put("/lib/**", "anon");//静态资源不拦截
        filterMap.put("/images/**", "anon");//静态资源不拦截
        filterMap.put("/css/**", "anon");//静态资源不拦截
        filterMap.put("/api/**", "anon");//静态资源不拦截

        //Swagger不拦截
        filterMap.put("/swagger-ui.html", "anon");
        filterMap.put("/swagger-resources/**", "anon");
        filterMap.put("/v2/api-docs/**", "anon");
        filterMap.put("/webjars/springfox-swagger-ui/**", "anon");

        //Android请求放行
        filterMap.put("/android/**", "anon");
        //WebSocket放行
        filterMap.put("/websocket/*", "anon");

        filterMap.put("/manager/cedit", "authc");//修改个人信息无需管理员权限
        filterMap.put("/manager/*", "perms[manager:manager]");//需要管理员权限
        filterMap.put("/user/*", "perms[manager:user]");//需要用户权限
        filterMap.put("/shop/*", "perms[manager:shop]");//需要店铺权限
        filterMap.put("/forum/*", "perms[manager:forum]");//需要论坛权限
        filterMap.put("/**", "authc");


        bean.setFilterChainDefinitionMap(filterMap);

        //设置权限不足的跳转页面
        bean.setUnauthorizedUrl("/to403");

        //设置登录的请求，当被拦截时，跳转到登录页面
        bean.setLoginUrl("/toLogin");

        return bean;
    }

    @Bean
    public DefaultWebSessionManager mySessionManager(){
        DefaultWebSessionManager defaultSessionManager = new DefaultWebSessionManager();
        //将sessionIdUrlRewritingEnabled属性设置成false
        defaultSessionManager.setSessionIdUrlRewritingEnabled(false);
        return defaultSessionManager;
    }

    @Resource
    DefaultWebSessionManager defaultSessionManager;
    //2. DefaultWebSecurityManager，
    // 注解@Qualifier 是按名称进行注入，Spring会通过userRealm()方法返回一个UserRealm对象。
    @Bean(name = "securityManager")
    public DefaultWebSecurityManager getDefaultWebSecurityManager(@Qualifier("managerRealm") ManagerRealm managerRealm,
            @Qualifier("rememberMeManager")RememberMeManager rememberMeManager){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        //关联ManagerRealm
        securityManager.setRealm(managerRealm);
        //解决第一次jSession问题
        securityManager.setSessionManager(defaultSessionManager);
        //注入记住我管理器
        securityManager.setRememberMeManager(rememberMeManager);
        return securityManager;
    }

    //1. 创建Realm对象，返回Bean，需要先自定义类。
    @Bean(name = "managerRealm")//可省略name，默认就是通过managerRealm()方法名来获取
    public ManagerRealm managerRealm(){
        return new ManagerRealm();
    }


    //利用ShiroDialect来整合Thymeleaf，这个方法是为了在前端使用Shiro命名空间。
    @Bean
    public ShiroDialect getShiroDialect(){
        return new ShiroDialect();
    }
}