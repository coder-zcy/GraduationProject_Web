package com.zcy.config;

import com.zcy.entity.Manager;
import com.zcy.service.impl.ManagerServiceImpl;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;

//自定义的ManagerRealm
public class ManagerRealm extends AuthorizingRealm {
    @Autowired
    ManagerServiceImpl managerService;

    //授权（需要Manager有相应权限才能访问）
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(
            PrincipalCollection principals) {

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        //对当前管理员进行判断并授予相应权限
        Subject subject = SecurityUtils.getSubject();//获得当前用户
        Manager currentManager = (Manager) subject.getPrincipal();//拿到当前Manager对象，是认证里传过来的

        //该Manager是从数据库中读取的，Manager的Perms是该用户拥有的权限。
        String[] strings = currentManager.getAuthority().split("\\|");
        if(strings!=null){
            ArrayList<String> arrayList = new ArrayList<>();
            for (String string : strings) {
                arrayList.add(string);
            }
            //授予当前用户的权限
            info.addStringPermissions(arrayList);
        }

        return info;
    }


    //认证（判断用户名和密码是否正确）
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(
            AuthenticationToken token) throws AuthenticationException {

        UsernamePasswordToken userToken = (UsernamePasswordToken)token;

        //用户名认证
        Manager manager = managerService.getManagerByName(userToken.getUsername());
        if(manager == null){//用户是否存在
            return null;//如果用户名不存在，则返回null，即会抛出异常 UnknownAccountException
        }

        //密码认证，由Shiro自己完成。可以进行加密。第一个参数 可以让其他方法拿到
        return new SimpleAuthenticationInfo(manager, manager.getPassword(), "");
    }
}
