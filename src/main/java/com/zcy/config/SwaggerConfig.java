package com.zcy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

//@Configuration就是一个@Component
@Configuration
@EnableSwagger2//开启Swagger2
public class SwaggerConfig {
    //配置Swagger
    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                //.enable(false) //这样就关闭了Swagger，默认是打开的。
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.zcy.controller"))//扫描controller/user
                //.paths(PathSelectors.ant("/hello/2"))//此时/hello/1就不会被扫描
                .build();
    }

    //配置Swagger信息
    private ApiInfo apiInfo(){
        //作者信息
        Contact contact = new Contact("张城阳", "https://blog.csdn.net/qq_39763246", "1142729000@qq.com");
        //参数依次是：标题、描述、版本、服务条款、联系人、执照、执照链接
        return new ApiInfo(
                "张城阳的API文档",
                "毕业设计项目",
                "v1.0",
                null,
                contact,
                null,
                null,
                new ArrayList());
    }
}