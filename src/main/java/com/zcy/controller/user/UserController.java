package com.zcy.controller.user;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zcy.entity.Manager;
import com.zcy.entity.Posts;
import com.zcy.entity.User;
import com.zcy.service.*;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (User)表控制层
 *
 * @author makejava
 * @since 2021-04-23 17:55:33
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private ShopService shopService;

    @Resource
    private UserService userService;

    @Resource
    private PostsService postsService;

    @Resource
    private CommentService commentService;

    @Resource
    private PartnerService partnerService;

    @Resource
    private CommodityService commodityService;



    /**
     * 分页查询用户表
     * @param page 当前页
     * @param limit 页面大小
     * @return
     */
    @PostMapping("/get_user")
    @ResponseBody
    public String getUser(int page, int limit){
        List<User> users = userService.getUserByPage(page, limit);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        array.addAll(users);
        object.put("count", users.size());
        object.put("data", array);
        return object.toJSONString();
    }

    /**
     * 按条件搜索用户
     * @param user User的JSON
     * @return 返回用户的json
     */
    @PostMapping("/search_user")
    @ResponseBody
    public String searchUser(@RequestBody User user){
        List<User> users = userService.getUserByCondition(user);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        array.addAll(users);
        object.put("count", users.size());
        object.put("data", array);
        return object.toJSONString();
    }


    /**
     * 批量删除用户
     * @param map 前端JSON的键值对
     * @return 提示信息
     */
    @PostMapping("/delete_user")
    @ResponseBody
    public String deleteUser(@RequestParam Map<String, String> map){
        List<Integer> usersId = new ArrayList<>();
        for(int i = 0; i < map.size(); i++){
            usersId.add(Integer.parseInt(map.get(i+"")));
        }
        int i = userService.deleteUsersById(usersId);
        JSONObject object = new JSONObject();
        if (i > 0){
            object.put("code", 0);//成功
            object.put("msg", "success");//成功

            //删除对应店铺、贴子、商品、评论、关系
            for (Integer integer : usersId){
                commodityService.deleteCommoditiesByUserId(integer);
                shopService.deleteShopsByUserId(integer);
                postsService.deletePostsByAuthorId(integer);
                //删除该用户的评论
                commentService.deleteCommentsByUserId(integer);
                //删除该用户的被评论
                commentService.deleteCommentByTypeId(integer, 1);
                //处理关系
                partnerService.deletePartnerByKeepId(integer);
                partnerService.closePartnerByPartner1Id(integer);
                partnerService.closePartnerByPartner2Id(integer);
            }
        }
        else{
            object.put("code", 1);//失败
            object.put("msg", "failure");//失败
        }
        return object.toJSONString();
    }

    /**
     * 添加用户
     * @param map 用户相关信息
     * @return {code: 0, msg: 'success'}
     */
    @PostMapping("/add_user")
    @ResponseBody
    public Map addUser(@RequestParam Map<String, String> map){
        Map<String, Object> result = new HashMap<>();
        User user = new User();
        user.setUserName(map.get("userName"));
        user.setPassword(map.get("password"));
        user.setPhoneNumber(map.get("phoneNumber"));
        user.setAvatar(map.get("avatar"));
        user.setSign(map.get("sign"));
        user.setSex(Integer.parseInt(map.get("sex")));
        user.setAddress(map.get("address"));
        user.setStudentId(map.get("studentId"));
        user.setSchool(map.get("school"));
        if (!map.get("point").equals(""))
            user.setPoint(Integer.parseInt(map.get("point")));
        user.setGrade(map.get("grade"));

        int i = userService.addUser(user);
        if (i > 0){
            result.put("code", 0);
            result.put("msg", "success");
        }else{
            result.put("code", 1);
            result.put("msg", "failure");
        }
        return result;
    }

    /**
     * 编辑用户
     * @param map 用户相关信息
     * @return {code: 0, msg: 'success'}
     */
    @PostMapping("/update_user")
    @ResponseBody
    public Map updateUser(@RequestParam Map<String, String> map){
        Map<String, Object> result = new HashMap<>();

        User user = new User();
        user.setUserId(Integer.parseInt(map.get("userId")));
        user.setUserName(map.get("userName"));
        user.setPassword(map.get("password"));
        user.setPhoneNumber(map.get("phoneNumber"));
        user.setAvatar(map.get("avatar"));
        user.setSign(map.get("sign"));
        user.setSex(Integer.parseInt(map.get("sex")));
        user.setAddress(map.get("address"));
        user.setPoint(Integer.parseInt(map.get("point")));
        user.setStudentId(map.get("studentId"));
        user.setSchool(map.get("school"));
        user.setGrade(map.get("grade"));

        int i = userService.updateUserById(user);
        if (i > 0){
            result.put("code", 0);
            result.put("msg", "success");
        }else{
            result.put("code", 1);
            result.put("msg", "failure");
        }
        return result;
    }

}
