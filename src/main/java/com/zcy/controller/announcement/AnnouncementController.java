package com.zcy.controller.announcement;

import com.zcy.entity.Announcement;
import com.zcy.entity.Manager;
import com.zcy.entity.Posts;
import com.zcy.entity.User;
import com.zcy.service.AnnouncementService;
import com.zcy.service.ManagerService;
import com.zcy.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * (Announcement)表控制层
 *
 * @author makejava
 * @since 2021-04-23 17:55:31
 */
@RestController
@RequestMapping("announcement")
public class AnnouncementController {

    @Resource
    private AnnouncementService announcementService;
    @Resource
    private ManagerService managerService;
    @Resource
    private UserService userService;

    @PostMapping("/add_announcement")
    @ResponseBody
    public Map addAnnouncement(@RequestParam Map<String, String> map){
        Map<String, Object> result = new HashMap<>();
        Announcement announcement = new Announcement();

        if (!map.get("id").equals("") && !map.get("type").equals("")){
            if (map.get("type").equals("0")){
                Manager manager = new Manager();
                manager.setManagerId(Integer.parseInt(map.get("id")));
                if (managerService.getManagerByCondition(manager).size() > 0)
                    announcement.setId(manager.getManagerId());
                else{
                    result.put("code", 2);
                    result.put("msg", "管理员不存在");
                    return result;
                }
            }else if (map.get("type").equals("1")){
                User user = new User();
                user.setUserId(Integer.parseInt(map.get("id")));
                if (userService.getUserByCondition(user).size() > 0)
                    announcement.setId(user.getUserId());
                else{
                    result.put("code", 2);
                    result.put("msg", "用户不存在");
                    return result;
                }
            }

        }

        if (map.get("isDeal").equals("1"))
            announcement.setIsDeal(1);
        announcement.setAnnoTitle(map.get("annoTitle"));
        announcement.setAnnoContent(map.get("annoContent"));

        int i = announcementService.add(announcement);
        if (i > 0){
            result.put("code", 0);
            result.put("msg", "success");
        }else{
            result.put("code", 1);
            result.put("msg", "failure");
        }
        return result;
    }

    @PostMapping("/update_announcement")
    @ResponseBody
    public Map updateAnnouncement(@RequestParam Map<String, String> map){
        Map<String, Object> result = new HashMap<>();
        Announcement announcement = new Announcement();

        if (!map.get("annoId").equals(""))
            announcement.setAnnoId(Integer.parseInt(map.get("annoId")));

        if (!map.get("id").equals("") && !map.get("type").equals("")){
            if (map.get("type").equals("0")){
                Manager manager = new Manager();
                manager.setManagerId(Integer.parseInt(map.get("id")));
                if (managerService.getManagerByCondition(manager).size() > 0)
                    announcement.setId(manager.getManagerId());
                else{
                    result.put("code", 2);
                    result.put("msg", "管理员不存在");
                    return result;
                }
            }else if (map.get("type").equals("1")){
                User user = new User();
                user.setUserId(Integer.parseInt(map.get("id")));
                if (userService.getUserByCondition(user).size() > 0)
                    announcement.setId(user.getUserId());
                else{
                    result.put("code", 2);
                    result.put("msg", "用户不存在");
                    return result;
                }
            }

        }

        if (map.get("isDeal").equals("1"))
            announcement.setIsDeal(1);
        announcement.setAnnoTitle(map.get("annoTitle"));
        announcement.setAnnoContent(map.get("annoContent"));

        int i = announcementService.updateByAnId(announcement);
        if (i > 0){
            result.put("code", 0);
            result.put("msg", "success");
        }else{
            result.put("code", 1);
            result.put("msg", "failure");
        }
        return result;
    }
}
