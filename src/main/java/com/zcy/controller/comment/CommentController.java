package com.zcy.controller.comment;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zcy.entity.Comment;
import com.zcy.entity.Commodity;
import com.zcy.entity.Posts;
import com.zcy.entity.User;
import com.zcy.service.*;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (Comment)表控制层
 *
 * @author makejava
 * @since 2021-04-23 17:55:31
 */
@RestController
@RequestMapping("comment")
public class CommentController  {


    @Resource
    private UserService userService;
    @Resource
    private PostsService postsService;
    @Resource
    private CommodityService commodityService;
    @Resource
    private CommentService commentService;


    /**
     * 分页查询评论表
     * @param page 当前页
     * @param limit 页面大小
     * @return
     */
    @PostMapping("/get_comment")
    @ResponseBody
    public String getComment(int page, int limit){
        List<Comment> comments = commentService.getCommentByPage(page, limit);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        array.addAll(comments);
        object.put("count", comments.size());
        object.put("data", array);
        return object.toJSONString();
    }

    /**
     * 按条件搜索评论
     * @param comment comment的JSON
     * @return 返回评论的json
     */
    @PostMapping("/search_comment")
    @ResponseBody
    public String searchComment(@RequestBody Comment comment){
        List<Comment> shops = commentService.getCommentByCondition(comment);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        array.addAll(shops);
        object.put("count", shops.size());
        object.put("data", array);
        return object.toJSONString();
    }

    /**
     * 批量删除评论
     * @param map 前端JSON的键值对
     * @return 提示信息
     */
    @PostMapping("/delete_comment")
    @ResponseBody
    public String deleteComment(@RequestParam Map<String, String> map){
        List<Integer> commentsId = new ArrayList<>();
        for(int i = 0; i < map.size(); i++){
            commentsId.add(Integer.parseInt(map.get(i+"")));
        }
        int i = commentService.deleteCommentsById(commentsId);
        JSONObject object = new JSONObject();
        if (i > 0){
            object.put("code", 0);//成功
            object.put("msg", "success");//成功
        }
        else{
            object.put("code", 1);//失败
            object.put("msg", "failure");//失败
        }
        return object.toJSONString();
    }

    /**
     * 添加评论
     * @param map 评论相关信息
     * @return {code: 0, msg: 'success'}
     */
    @PostMapping("/add_comment")
    @ResponseBody
    public Map addComment(@RequestParam Map<String, String> map){
        Map<String, Object> result = new HashMap<>();
        Comment comment = new Comment();
        //设置发起人ID
        if (!map.get("userId").equals("")){
            User user = new User();
            user.setUserId(Integer.parseInt(map.get("userId")));
            if (userService.getUserByCondition(user).size() > 0)
                comment.setUserId(user.getUserId());
            else{
                result.put("code", 2);
                result.put("msg", "发起人用户的ID不存在");
                return result;
            }
        }

        //设置是否已读
        if (!map.get("read").equals(""))
            comment.setIsRead(Integer.parseInt(map.get("read")));

        //设置目标ID
        if (!map.get("type").equals("")){
            int type = Integer.parseInt(map.get("type"));
            comment.setType(type);
            //用户
            if (type == 0){
                User user = new User();
                user.setUserId(Integer.parseInt(map.get("id")));
                if (userService.getUserByCondition(user).size() > 0)
                    comment.setId(user.getUserId());
                else{
                    result.put("code", 2);
                    result.put("msg", "对象用户的ID不存在");
                    return result;
                }
            }else if (type == 1){
                //贴子
                Posts posts = new Posts();
                posts.setPostId(Integer.parseInt(map.get("id")));
                if (postsService.getPostsByCondition(posts).size() > 0)
                    comment.setId(posts.getPostId());
                else{
                    result.put("code", 2);
                    result.put("msg", "对象贴子的ID不存在");
                    return result;
                }
            }else if (type == 2){
                //商品
                Commodity commodity = new Commodity();
                commodity.setCmodId(Integer.parseInt(map.get("id")));
                if (commodityService.getCommodityByCondition(commodity).size() > 0)
                    comment.setId(commodity.getCmodId());
                else{
                    result.put("code", 2);
                    result.put("msg", "对象商品的ID不存在");
                    return result;
                }
            }
        }

        comment.setCmenContent(map.get("cmenContent"));
        comment.setCmenImg(map.get("cmenImg"));

        int i = commentService.addComment(comment);
        if (i > 0){
            result.put("code", 0);
            result.put("msg", "success");
        }else{
            result.put("code", 1);
            result.put("msg", "failure");
        }
        return result;
    }

    /**
     * 编辑评论
     * @param map 评论相关信息
     * @return {code: 0, msg: 'success'}
     */
    @PostMapping("/update_comment")
    @ResponseBody
    public Map updateComment(@RequestParam Map<String, String> map){
        Map<String, Object> result = new HashMap<>();
        Comment comment = new Comment();
        comment.setCmenId(Integer.parseInt(map.get("cmenId")));
        comment.setCmenContent(map.get("cmenContent"));
        if (!map.get("read").equals(""))
            comment.setIsRead(Integer.parseInt(map.get("read")));
        int i = commentService.updateCommentById(comment);
        if (i > 0){
            result.put("code", 0);
            result.put("msg", "success");
        }else{
            result.put("code", 1);
            result.put("msg", "failure");
        }
        return result;
    }
}
