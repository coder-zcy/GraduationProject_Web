package com.zcy.controller.partner;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.zcy.entity.*;
import com.zcy.service.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.Part;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (Partner)表控制层
 *
 * @author makejava
 * @since 2021-04-23 17:55:33
 */
@RestController
@RequestMapping("partner")
public class PartnerController {

    @Resource
    private PartnerService partnerService;
    @Resource
    private UserService userService;


    /**
     * 分页查询关系表
     * @param page 当前页
     * @param limit 页面大小
     * @return
     */
    @PostMapping("/get_partner")
    @ResponseBody
    public String getPartner(int page, int limit){
        List<Partner> partners = partnerService.getPartnerByPage(page, limit);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        array.addAll(partners);
        object.put("count", partners.size());
        object.put("data", array);
        return object.toJSONString();
    }

    /**
     * 按条件搜索关系
     * @param partner comment的JSON
     * @return 返回评论的json
     */
    @PostMapping("/search_partner")
    @ResponseBody
    public String searchShop(@RequestBody Partner partner){
        List<Partner> partners = partnerService.getPartnerByCondition(partner);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        array.addAll(partners);
        object.put("count", partners.size());
        object.put("data", array);
        return object.toJSONString();
    }

    /**
     * 编辑关系
     * @param map 关系相关信息
     * @return {code: 0, msg: 'success'}
     */
    @PostMapping("/update_partner")
    @ResponseBody
    public Map updatePartner(@RequestParam Map<String, String> map){
        Map<String, Object> result = new HashMap<>();
        Partner partner = new Partner();
        partner.setShopkeeperId(Integer.parseInt(map.get("shopkeeperId")));

        if (!map.get("partner1Id").equals("")){
            int partner1Id = Integer.parseInt(map.get("partner1Id"));
            User user = new User();
            user.setUserId(partner1Id);
            if (userService.getUserByCondition(user).size() > 0)
                partner.setPartner1Id(partner1Id);
            else{
                result.put("code", 2);
                result.put("msg", "伙伴1的ID不存在");
                return result;
            }
        }
        if (!map.get("partner2Id").equals("")){
            int partner2Id = Integer.parseInt(map.get("partner2Id"));
            User user = new User();
            user.setUserId(partner2Id);
            if (userService.getUserByCondition(user).size() > 0)
                partner.setPartner2Id(partner2Id);
            else{
                result.put("code", 2);
                result.put("msg", "伙伴2的ID不存在");
                return result;
            }
        }


        int i = partnerService.updatePartnerByKeepId(partner);
        if (i > 0){
            result.put("code", 0);
            result.put("msg", "success");
        }else{
            result.put("code", 1);
            result.put("msg", "failure");
        }
        return result;
    }
}
