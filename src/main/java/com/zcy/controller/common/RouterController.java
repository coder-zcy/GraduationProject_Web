package com.zcy.controller.common;

import com.zcy.entity.Manager;
import com.zcy.service.ManagerService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class RouterController {
    @Resource
    private ManagerService managerService;

    /**
     * 输入 localhost:8080或者localhost:8080/index都会跳转到主页
     * @return 跳转位置
     */
    @RequestMapping({"/index", "/"})
    public String index(){
        return "index";
    }

    /**
     * 欢迎页主页
     * @return 跳转位置
     */
    @RequestMapping({"/welcome"})
    public String welcome(){
        return "welcome";
    }

    /**
     * 进入登录页面
     * @return 跳转位置
     */
    @RequestMapping("/toLogin")
    public String toLogin(){
        return "login/login";
    }

    /**
     *  退出登录，回到登陆页面，消除cookie
     * @param response
     * @return
     */
    @RequestMapping("/logout")
    public String logout(HttpServletResponse response){
        Subject subject = SecurityUtils.getSubject();
        if (subject.isAuthenticated()){
            subject.logout();
            Cookie cookie = new Cookie("rememberMe", null);
            cookie.setMaxAge(0);
            response.addCookie(cookie);
        }
        return "/login/login";
    }

    /**
     * 处理登录请求
     * @param username 用户名
     * @param password 密码
     * @param rememberMe 记住我
     * @param model 数据模型
     * @param request 用于获取session
     * @return
     */
    @PostMapping("/login")
    public String login(String username, String password, Boolean rememberMe,
            Model model, HttpServletRequest request){
        //获取Session
        HttpSession session = request.getSession();

        //获取当前用户，Subject对象就代表当前用户
        Subject subject = SecurityUtils.getSubject();
        //封装用户的登录数据(将前端传进来的用户名和密码放入token里)
        UsernamePasswordToken token = new UsernamePasswordToken(username, password, rememberMe);

        try {
            subject.login(token);
            Manager manager = managerService.getManagerByName(username);
            session.setAttribute("manager", manager);
            //登录成功则进入主页，失败则抛出异常并返回登录页
            return "index";
        }catch (UnknownAccountException e){
            //用户名不存在
            model.addAttribute("msg", "该管理员不存在");
            return "login/login";
        }catch (IncorrectCredentialsException e){
            //密码错误
            model.addAttribute("msg", "您的密码错误");
            return "login/login";
        }
    }

    /**
     * 进入403 权限不足页面
     * @return 跳转位置
     */
    @RequestMapping("/to403")
    public String to403(){
        return "error/403";
    }

    /**
     * 进入404 页面无法访问
     * @return 跳转位置
     */
    @RequestMapping("/to404")
    public String to404(){
        return "error/404";
    }

    /**
     * 跳转到管理员表页面
     * @return 跳转位置
     */
    @RequestMapping("/manager/toTable")
    public String toTableManager(){
        return "manager/table";
    }

    /**
     * 跳转到添加管理员页面
     * @return 跳转位置
     */
    @RequestMapping("/manager/toAdd")
    public String toAddManager(){
        return "manager/add";
    }

    /**
     * 跳转到编辑管理员页面
     * @return 跳转位置
     */
    @GetMapping("/manager/toEdit")
    public String toEditManager(){
        return "manager/edit";
    }

    /**
     * 跳转到编辑管理员页面
     * @return 跳转位置
     */
    @GetMapping("/manager/toCEdit")
    public String toCEditManager(){
        return "manager/cedit";
    }

    /**
     * 跳转到用户表页面
     * @return 跳转位置
     */
    @RequestMapping("/user/toTable")
    public String toTableUser(){
        return "user/table";
    }

    /**
     * 跳转到添加用户页面
     * @return 跳转位置
     */
    @RequestMapping("/user/toAdd")
    public String toAddUser(){
        return "user/add";
    }

    /**
     * 跳转到编辑用户页面
     * @return 跳转位置
     */
    @GetMapping("/user/toEdit")
    public String toEditUser(){
        return "user/edit";
    }


    /**
     * 跳转到商店表页面
     * @return 跳转位置
     */
    @RequestMapping("/shop/toTable")
    public String toTableShop(){
        return "shop/table";
    }

    /**
     * 跳转到添加商店页面
     * @return 跳转位置
     */
    @RequestMapping("/shop/toAdd")
    public String toAddShop(){
        return "shop/add";
    }

    /**
     * 跳转到编辑商店页面
     * @return 跳转位置
     */
    @GetMapping("/shop/toEdit")
    public String toEditShop(){
        return "shop/edit";
    }


    /**
     * 跳转到商品表页面
     * @return 跳转位置
     */
    @RequestMapping("/commodity/toTable")
    public String toTableCommodity(){
        return "commodity/table";
    }

    /**
     * 跳转到添加商店页面
     * @return 跳转位置
     */
    @RequestMapping("/commodity/toAdd")
    public String toAddCommodity(){
        return "commodity/add";
    }

    /**
     * 跳转到编辑商品页面
     * @return 跳转位置
     */
    @GetMapping("/commodity/toEdit")
    public String toEditCommodity(){
        return "commodity/edit";
    }


    /**
     * 跳转到贴子表页面
     * @return 跳转位置
     */
    @RequestMapping("/posts/toTable")
    public String toTablePosts(){
        return "posts/table";
    }

    /**
     * 跳转到添加贴子页面
     * @return 跳转位置
     */
    @RequestMapping("/posts/toAdd")
    public String toAddPosts(){
        return "posts/add";
    }

    /**
     * 跳转到编辑贴子页面
     * @return 跳转位置
     */
    @GetMapping("/posts/toEdit")
    public String toEditPosts(){
        return "posts/edit";
    }

    /**
     * 跳转到关系表页面
     * @return 跳转位置
     */
    @RequestMapping("/partner/toTable")
    public String toTablePartner(){
        return "partner/table";
    }

    /**
     * 跳转到编辑关系页面
     * @return 跳转位置
     */
    @GetMapping("/partner/toEdit")
    public String toEditPartner(){
        return "partner/edit";
    }


    /**
     * 跳转到评论表页面
     * @return 跳转位置
     */
    @RequestMapping("/comment/toTable")
    public String toTableComment(){
        return "comment/table";
    }

    /**
     * 跳转到添加评论页面
     * @return 跳转位置
     */
    @RequestMapping("/comment/toAdd")
    public String toAddComment(){
        return "comment/add";
    }

    /**
     * 跳转到评论关系页面
     * @return 跳转位置
     */
    @GetMapping("/comment/toEdit")
    public String toEditComment(){
        return "comment/edit";
    }

    /**
     * 跳转到发布公告页面
     * @return 跳转位置
     */
    @GetMapping("/announcement/toAdd")
    public String toAddAnnouncement(){
        return "announcement/add";
    }
}
