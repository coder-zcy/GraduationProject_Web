package com.zcy.controller.common;

import com.zcy.dao.CommodityDao;
import com.zcy.dao.PostsDao;
import com.zcy.dao.ShopDao;
import com.zcy.dao.UserDao;
import com.zcy.entity.Manager;
import com.zcy.service.*;
import com.zcy.tool.QiniuCloudUtil;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
public class CommonController {
    @Resource
    private UserService userService;

    @Resource
    private ManagerService managerService;

    @Resource
    private CommodityService commodityService;

    @Resource
    private ShopService shopService;

    @Resource
    private PostsService postsService;

    @Resource
    private CommentService commentService;



    /**
     * 上传图片
     * @param image 图片
     * @param map 上传类型
     * @return json
     */
    @RequestMapping("/uploadImg")
    @ResponseBody
    public Map upload(MultipartFile image, @RequestParam Map<String, Object> map){
        Map<String, Object> result = new HashMap<>();
        if(image.isEmpty()){
            result.put("code", 1);
            result.put("msg", "文件为空");
        }
        try {
            byte[] bytes = image.getBytes();

            try {
                //使用Base64方式上传图片
                String url = QiniuCloudUtil.uploadImage(bytes, map.get("type")+"/"+UUID.randomUUID().toString()+".jpg");
                result.put("code", 0);
                result.put("msg", "上传成功");
                result.put("src", url);
            }catch (Exception e) {
                e.getStackTrace();
            }
            return result;
        }catch (IOException e){
            result.put("code", 2);
            result.put("msg", "文件上传异常");
            return result;
        }
    }

    /**
     * 删除图片
     * @param src 图片路径
     * @return
     */
    @RequestMapping("/deleteImg")
    @ResponseBody
    public Map delete(String src){
        Map<String, Object> result = new HashMap<>();
        try {
            QiniuCloudUtil.deleteImage(src);
            result.put("code", 0);
            result.put("msg", "删除成功");
        }catch (Exception e) {
            e.getStackTrace();
            result.put("code", 1);
            result.put("msg", "删除失败");
        }
        return result;
    }

    /**
     * 获得统计数据
     * @return
     */
    @PostMapping("/statistics")
    @ResponseBody
    public Map getStatistics(){
        Map<String, String> map = new HashMap<>();
        map.put("userTotal", userService.getTotal().toString());
        map.put("managerTotal", managerService.getTotal().toString());
        map.put("shopTotal", shopService.getTotal().toString());
        map.put("commodityTotal", commentService.getTotal().toString());
        map.put("commentTotal", commentService.getTotal().toString());
        map.put("postTotal", postsService.getTotal().toString());
        return map;
    }
}
