package com.zcy.controller.shop;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zcy.entity.Commodity;
import com.zcy.entity.Partner;
import com.zcy.entity.Shop;
import com.zcy.entity.User;
import com.zcy.service.CommodityService;
import com.zcy.service.PartnerService;
import com.zcy.service.ShopService;
import com.zcy.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (Shop)表控制层
 *
 * @author makejava
 * @since 2021-04-23 17:55:33
 */
@RestController
@RequestMapping("shop")
public class ShopController {

    @Resource
    private ShopService shopService;
    @Resource
    private UserService userService;
    @Resource
    private CommodityService commodityService;
    @Resource
    private PartnerService partnerService;


    /**
     * 分页查询店铺表
     * @param page 当前页
     * @param limit 页面大小
     * @return
     */
    @PostMapping("/get_shop")
    @ResponseBody
    public String getShop(int page, int limit){
        List<Shop> shops = shopService.getShopByPage(page, limit);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        array.addAll(shops);
        object.put("count", shops.size());
        object.put("data", array);
        return object.toJSONString();
    }

    /**
     * 按条件搜索店铺
     * @param shop Shop的JSON
     * @return 返回店铺的json
     */
    @PostMapping("/search_shop")
    @ResponseBody
    public String searchShop(@RequestBody Shop shop){
        List<Shop> shops = shopService.getShopByCondition(shop);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        array.addAll(shops);
        object.put("count", shops.size());
        object.put("data", array);
        return object.toJSONString();
    }

    /**
     * 批量删除店铺
     * @param map 前端JSON的键值对
     * @return 提示信息
     */
    @PostMapping("/delete_shop")
    @ResponseBody
    public String deleteShop(@RequestParam Map<String, String> map){
        List<Integer> shopsId = new ArrayList<>();
        for(int i = 0; i < map.size(); i++){
            shopsId.add(Integer.parseInt(map.get(i+"")));
        }
        int i = shopService.deleteShopsById(shopsId);
        JSONObject object = new JSONObject();
        if (i > 0){
            object.put("code", 0);//成功
            object.put("msg", "success");//成功

            //删除对应商品并清除关系
            for (Integer integer : shopsId){
                commodityService.deleteCommoditiesByShopId(integer);
                partnerService.deletePartnerByKeepId(integer);
            }
        }
        else{
            object.put("code", 1);//失败
            object.put("msg", "failure");//失败
        }
        return object.toJSONString();
    }

    /**
     * 添加店铺
     * @param map 店铺相关信息
     * @return {code: 0, msg: 'success'}
     */
    @PostMapping("/add_shop")
    @ResponseBody
    public Map addShop(@RequestParam Map<String, String> map){
        Map<String, Object> result = new HashMap<>();
        Shop shop = new Shop();
        if (!map.get("userId").equals("")){
            User user = new User();
            user.setUserId(Integer.parseInt(map.get("userId")));
            if (userService.getUserByCondition(user).size() > 0)
                shop.setUserId(user.getUserId());
            else{
                result.put("code", 2);
                result.put("msg", "店主不存在");
                return result;
            }
        }

        shop.setShopName(map.get("shopName"));
        shop.setShopIntro(map.get("shopIntro"));
        shop.setKeywords(map.get("keywords"));
        shop.setShopImg(map.get("shopImg"));

        int i = shopService.addShop(shop);
        if (i > 0){
            result.put("code", 0);
            result.put("msg", "success");
            //添加关系
            Partner partner = new Partner();
            partner.setShopkeeperId(shop.getUserId());
            partner.setShopId(shop.getShopId());
            partnerService.addPartner(partner);
        }else{
            result.put("code", 1);
            result.put("msg", "failure");
        }
        return result;
    }

    /**
     * 编辑店铺
     * @param map 店铺相关信息
     * @return {code: 0, msg: 'success'}
     */
    @PostMapping("/update_shop")
    @ResponseBody
    public Map updateShop(@RequestParam Map<String, String> map){
        Map<String, Object> result = new HashMap<>();
        Shop shop = new Shop();
        shop.setShopId(Integer.parseInt(map.get("shopId")));
        shop.setShopName(map.get("shopName"));
        shop.setShopIntro(map.get("shopIntro"));
        shop.setKeywords(map.get("keywords"));
        shop.setShopImg(map.get("shopImg"));

        int i = shopService.updateShopById(shop);
        if (i > 0){
            result.put("code", 0);
            result.put("msg", "success");

        }else{
            result.put("code", 1);
            result.put("msg", "failure");
        }
        return result;
    }
}
