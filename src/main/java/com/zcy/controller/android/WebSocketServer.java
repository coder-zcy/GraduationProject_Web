package com.zcy.controller.android;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

//本机 172.20.10.5:9090/websocket/xxxx
@ServerEndpoint("/websocket/{userId}")//客户端URI访问的路径
@Component
public class WebSocketServer {
    private Session session; // 与客户端连接的会话，用于发送数据


    /**
     * 连接成功时调用，并保存用户上线记录
     * @param session
     * @param userId
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("userId") String userId){
        //记录上线用户，并保存会话，放入会话池
        this.session = session;
        SessionPool.sessionMap.put(userId, session);
    }

    /**
     * 关闭会话连接
     * @param session 会话
     * @throws IOException
     */
    @OnClose
    public void onClose(Session session) throws IOException{
        SessionPool.close(session.getId());
        session.close();
    }

    /**
     * 收到客户端消息后调用的方法
     * @param message 客户端的消息
     * @param session 客户端的会话
     */
    @OnMessage
    public void onMessage(String message, Session session){
        SessionPool.sendMessage(message);
    }
}
