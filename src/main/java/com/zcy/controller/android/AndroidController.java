package com.zcy.controller.android;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mysql.cj.util.StringUtils;
import com.zcy.entity.*;
import com.zcy.service.*;
import com.zcy.tool.QiniuCloudUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("android")
public class AndroidController {

    @Resource
    private ShopService shopService;

    @Resource
    private UserService userService;

    @Resource
    private PostsService postsService;

    @Resource
    private CommentService commentService;

    @Resource
    private PartnerService partnerService;

    @Resource
    private CommodityService commodityService;

    @GetMapping("/get_token")
    public Map getToken(){
        Map<String, String> map = new HashMap<>();
        map.put("token", QiniuCloudUtil.getToken());
        return map;
    }

    /**
     * 用户登录
     * @param username 用户名
     * @param password 密码
     * @param phoneNumber 电话
     * @return
     */
    @PostMapping("/login")
    public Map userLogin(String username, String password, String phoneNumber){
        Map<String, Object> json = new HashMap<>();

        User user = new User();
        User resultUser = new User();
        if (password != null && (username!=null || phoneNumber != null)){
            user.setUserName(username);
            user.setPassword(password);
            user.setPhoneNumber(phoneNumber);
            resultUser = userService.userLogin(user);
            if (resultUser != null){
                Integer shopId = partnerService.getShopIdByUserId(resultUser.getUserId());
                if (shopId != null){
                    Shop shop = shopService.getShopById(shopId);
                    json.put("hasShop", JSONObject.toJSONString(shop));
                }
                json.put("code", "0");
                json.put("msg", "登录成功");
                json.put("data", resultUser);
            }else{
                json.put("code", "2");
                json.put("msg", "登录失败");
            }
        }else {
            json.put("code", "1");
            json.put("msg", "信息不完整");
        }
        return json;
    }

    /**
     * 注册
     * @param user 注册信息
     * @return
     */
    @PostMapping("/register")
    public Map userRegister(User user){
        Map<String, Object> json = new HashMap<>();

        int i = userService.addUser(user);
        if (i > 0){
            json.put("code", 0);
            json.put("msg", "success");
        }else {
            json.put("code", 1);
            json.put("msg", "failure");
        }
        return json;
    }

    /**
     * 通过商品名或商品类型 搜索 商品
     * @param commodityName 商品名
     * @param commodityType 商品类型
     * @return
     */
    @PostMapping("/get_commodity")
    public Map getCommodity(String commodityName, String commodityType){
        Commodity commodity = new Commodity();
        Map<String, Object> json = new HashMap<>();

        if (commodityName != null)
            commodity.setCmodName(commodityName);
        if (commodityType != null)
            commodity.setType(commodityType);

        List<Commodity> list = commodityService.getCommodityByCondition(commodity);
        if (list.size() > 0){
            json.put("code", 0);
            json.put("msg", "success");
            json.put("data", list);
        }else {
            json.put("code", 1);
            json.put("msg", "failure");
        }
        return json;
    }

    /**
     * 通过店铺名或店主名或关键词 搜索 店铺
     * @param shopName 店铺名
     * @param userName 店主名
     * @param keyword 店铺关键词
     * @return
     */
    @PostMapping("/get_shop")
    public Map getShop(String shopName, String userName,String userId, String keyword){
        Shop shop = new Shop();
        Map<String, Object> json = new HashMap<>();

        if (!StringUtils.isNullOrEmpty(shopName))
            shop.setShopName(shopName);
        if (!StringUtils.isNullOrEmpty(userName)){
            User user = userService.getUserByName(userName);
            if (user != null) {
                shop.setUserId(user.getUserId());
                json.put("isPartner", partnerService.isPartner(Integer.parseInt(userId)));
                json.put("partner1", partnerService.getPartner1IdByUserId(Integer.parseInt(userId)));
                json.put("partner2", partnerService.getPartner2IdByUserId(Integer.parseInt(userId)));
            }
        }
        //通过用户ID 获取店铺数据
        if (!StringUtils.isNullOrEmpty(userId)){
            Integer shopId = partnerService.getShopIdByUserId(Integer.parseInt(userId));
            shop.setShopId(shopId);
        }
        if (!StringUtils.isNullOrEmpty(keyword))
            shop.setKeywords(keyword);

        List<Shop> list = shopService.getShopByCondition(shop);
        if (list.size() > 0){
            json.put("code", 0);
            json.put("msg", "success");
            json.put("data", list);
        }else {
            json.put("code", 1);
            json.put("msg", "failure");
        }
        return json;
    }


    /**
     * 创建个人店铺
     * @param userId
     * @param shopName
     * @param shopIntro
     * @param keyword
     * @param shopImg
     * @param partner1
     * @param partner2
     * @return
     */
    @PostMapping("/add_shop")
    public Map addShop(String userId, String shopName, String shopIntro, String keyword, String shopImg, String partner1, String partner2){
        Map<String, Object> json = new HashMap<>();
        Shop shop =  new Shop();
        shop.setUserId(Integer.parseInt(userId));
        shop.setShopName(shopName);
        shop.setShopIntro(shopIntro);
        shop.setKeywords(keyword);
        shop.setShopImg(shopImg);

        if (!partnerService.hasShop(shop.getUserId())){
            if (shopService.addShop(shop) > 0){
                Partner partner = new Partner();
                partner.setShopkeeperId(Integer.parseInt(userId));
                partner.setShopId(shopService.getShopByName(shopName).getShopId());
                if(!StringUtils.isNullOrEmpty(partner1)){
                    User user = userService.getUserByName(partner1);
                    partner.setPartner1Id(user.getUserId());
                }
                if(!StringUtils.isNullOrEmpty(partner2)){
                    User user = userService.getUserByName(partner2);
                    partner.setPartner2Id(user.getUserId());
                }
                if(partnerService.addPartner(partner)>0){
                    json.put("code", 0);
                    json.put("msg", "success");
                    return json;
                }
            }
        }
        json.put("code", 1);
        json.put("msg", "failure");
        return json;
    }

    /**
     * 更新个人店铺
     * @param userId
     * @param shopName
     * @param shopIntro
     * @param keyword
     * @param shopImg
     * @param partner1
     * @param partner2
     * @return
     */
    @PostMapping("/update_shop")
    public Map updateShop(String isPartner, String userId, String shopName, String shopIntro, String keyword, String shopImg, String partner1, String partner2){
        Map<String, Object> json = new HashMap<>();
        Shop shop =  new Shop();
        shop.setUserId(Integer.parseInt(userId));
        shop.setShopName(shopName);
        shop.setShopIntro(shopIntro);
        shop.setKeywords(keyword);
        shop.setShopImg(shopImg);

        if (partnerService.hasShop(shop.getUserId())){
            if (shopService.updateShop(shop) > 0){
                Partner partner = new Partner();
                partner.setShopkeeperId(Integer.parseInt(userId));
                partner.setShopId(shopService.getShopByName(shopName).getShopId());
                if (isPartner.equals("is")){
                    if(!StringUtils.isNullOrEmpty(partner1)){
                        User user = userService.getUserByName(partner1);
                        partner.setPartner1Id(user.getUserId());
                    }
                    if(!StringUtils.isNullOrEmpty(partner2)){
                        User user = userService.getUserByName(partner2);
                        partner.setPartner2Id(user.getUserId());
                    }
                }
                if(partnerService.updatePartnerByKeepId(partner)>0){
                    json.put("code", 0);
                    json.put("msg", "success");
                    return json;
                }
            }
        }
        json.put("code", 1);
        json.put("msg", "failure");
        return json;
    }

    /**
     * 添加商品
     * @param commodities 商品列表JSON
     * @return
     */
    @PostMapping("/add_commodity")
    public Map addCommodity(String commodities){
        Map<String, Object> json = new HashMap<>();
        List<Commodity> list = JSON.parseArray(commodities, Commodity.class);
        if (list != null){
            Shop shop = shopService.getShopByUserId(list.get(0).getUserId());
            for (Commodity commodity : list){
                commodity.setShopId(shop.getShopId());
                commodityService.addCommodity(commodity);
            }
            json.put("code", 0);
            json.put("msg", "success");
        }else{
            json.put("code", 1);
            json.put("msg", "failure");
        }
        return json;
    }


    /**
     * 获取 user1 和 user2 的所有交流信息（用于店铺交流）
     * @param userId1 对方ID
     * @param userId2 自己ID
     * @return
     */
    @PostMapping("/get_comment_user")
    public Map getCommentToUser(String userId1, String userId2){
        Map<String, Object> json = new HashMap<>();
        List<Comment> comments = null;
        if (!StringUtils.isNullOrEmpty(userId1) && !StringUtils.isNullOrEmpty(userId2))
             comments = commentService.getCommentUTU(Integer.parseInt(userId1), Integer.parseInt(userId2));
        if (comments != null){
            json.put("code", 0);
            json.put("msg", "success");
            json.put("data", comments);
        }else {
            json.put("code", 1);
            json.put("msg", "failure");
        }
        return json;
    }

    /**
     * 获得指定商品的相关评论，评论图片是商品图
     * @param commodityId 商品ID
     * @return
     */
    @PostMapping("/get_comment_commodity")
    public Map getCommentToUser(String commodityId){
        Map<String, Object> json = new HashMap<>();
        List<Comment> comments = null;
        if (!StringUtils.isNullOrEmpty(commodityId))
            comments = commentService.getCommentUTC(Integer.parseInt(commodityId));
        if (comments != null){
            json.put("code", 0);
            json.put("msg", "success");
            json.put("data", comments);
        }else {
            json.put("code", 1);
            json.put("msg", "failure");
        }
        return json;
    }


    @PostMapping("/add_comment_commodity")
    public Map addCommentUTC(String userId, String commodityId, String content){
        Map<String, Object> json = new HashMap<>();
        int i = 0;
        Comment comment = new Comment();
        if (!StringUtils.isNullOrEmpty(userId) && !StringUtils.isNullOrEmpty(commodityId) && !StringUtils.isNullOrEmpty(content)){
            comment.setId(Integer.parseInt(commodityId));
            comment.setUserId(Integer.parseInt(userId));
            comment.setCmenContent(content);
            comment.setType(2);

            i = commentService.addComment(comment);
        }

        if (i > 0){
            json.put("code", 0);
            json.put("msg", "success");
        }else {
            json.put("code", 1);
            json.put("msg", "failure");
        }
        return json;
    }

    /**
     * 根据帖子名和类型获取
     * @param postTitle
     * @param postType
     * @return
     */
    @PostMapping("/get_post")
    public Map getPost(String postTitle, String postType){
        Map<String, Object> json = new HashMap<>();
        List<Posts> list = null;
        if (!StringUtils.isNullOrEmpty(postTitle))
            list = postsService.getPostsByTitle(postTitle);
        else if(!StringUtils.isNullOrEmpty(postType))
            list = postsService.getPostsByType(postType);
        else
            list = postsService.getPostsByCondition(new Posts());
        if (list != null){
            json.put("code", 0);
            json.put("msg", "success");
            json.put("data", list);
        }else {
            json.put("code", 1);
            json.put("msg", "failure");
        }
        return json;
    }

    @PostMapping("/add_post")
    public Map addPost(String title, String content, String reward, String img, String authorId){
        Map<String, Object> json = new HashMap<>();
        int i = 0;
        Posts post = new Posts();
        if (!StringUtils.isNullOrEmpty(authorId) && !StringUtils.isNullOrEmpty(title) && !StringUtils.isNullOrEmpty(content)){
            post.setPostTitle(title);
            post.setPostContent(content);
            post.setAuthorId(Integer.parseInt(authorId));

            if (!StringUtils.isNullOrEmpty(reward))
                post.setReward(Double.parseDouble(reward));
            if(!StringUtils.isNullOrEmpty(img))
                post.setPostImg(img);
            i = postsService.addPosts(post);
        }

        if (i > 0){
            json.put("code", 0);
            json.put("msg", "success");
        }else {
            json.put("code", 1);
            json.put("msg", "failure");
        }
        return json;
    }
}
