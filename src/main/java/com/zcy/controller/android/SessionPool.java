package com.zcy.controller.android;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zcy.entity.Comment;
import com.zcy.service.CommentService;
import com.zcy.service.impl.CommentServiceImpl;
import org.springframework.stereotype.Component;

import javax.websocket.Session;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class SessionPool {
    public static Map<String, Session> sessionMap = new ConcurrentHashMap<>();
    private static CommentService commentService = new CommentServiceImpl();

    /**
     * 发送消息给指定用户
     * @param data 消息，例如 {targetUserId:"1016", selfUserId:"1015", createTime:xxx, content:xxx, avatar:xxx}
     */
    public static void sendMessage(String data){
        
        //解析data
        JSONObject json1 = JSON.parseObject(data);
        Integer targetUserId = json1.getInteger("targetUserId");
        Integer selfUserId = json1.getInteger("selfUserId");
        Date createTime = json1.getDate("createTime");
        String content = json1.getString("content");
        String avatar = json1.getString("avatar");

        Session session = sessionMap.get(targetUserId.toString());

        //保存到Comment中
        Comment comment = new Comment();
        comment.setCmenContent(content);//保存消息内容
        comment.setUserId(selfUserId);//发送方ID
        comment.setId(targetUserId);//接收方ID
        comment.setType(0);//表明此次消息是 用户与用户
        comment.setCreateTime(createTime);//发表的时间
        comment.setCmenImg(avatar);//发送方头像的URL

        //将目标对象 给 自己发送的消息处理为已读
        commentService.setCommentRead(targetUserId, selfUserId);

        //判断用户是否上线
        if (session != null){
            //若用户已上线，则将消息发送给目标用户，且保存消息为已读
            comment.setIsRead(1);//消息已读
            //发给目标用户
            session.getAsyncRemote().sendText(data);
        }
        else
            comment.setIsRead(0);//目标用户未上线，消息未读
        commentService.addComment(comment);//写入数据库
    }

    /**
     * 关闭会话
     * @param sessionId
     * @throws IOException
     */
    public static void close(String sessionId) throws IOException {
        for (String userId:SessionPool.sessionMap.keySet()){
            Session session = SessionPool.sessionMap.get(userId);
            if (session.getId().equals(sessionId)){
                sessionMap.remove(userId);
                break;
            }
        }
    }

}
