package com.zcy.controller.commodity;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zcy.entity.Commodity;
import com.zcy.entity.Shop;
import com.zcy.service.CommentService;
import com.zcy.service.CommodityService;
import com.zcy.service.ShopService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (Commodity)表控制层
 *
 * @author makejava
 * @since 2021-04-23 17:55:31
 */
@RestController
@RequestMapping("commodity")
public class CommodityController {
    @Resource
    private ShopService shopService;
    @Resource
    private CommodityService commodityService;
    @Resource
    private CommentService commentService;

    /**
     * 分页查询商品
     * @param page 当前页
     * @param limit 页面大小
     * @return
     */
    @PostMapping("/get_commodity")
    @ResponseBody
    public String getCommodity(int page, int limit, Integer currentShopId){
        List<Commodity> commodities = commodityService.getCommodityByPage(page, limit, currentShopId);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        array.addAll(commodities);
        object.put("count", commodities.size());
        object.put("data", array);
        return object.toJSONString();
    }

    /**
     * 按条件搜索商品
     * @param commodity Commodity的JSON
     * @return 返回商品的json
     */
    @PostMapping("/search_commodity")
    @ResponseBody
    public String searchCommodity(@RequestBody Commodity commodity){
        List<Commodity> commodities = commodityService.getCommodityByCondition(commodity);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        array.addAll(commodities);
        object.put("count", commodities.size());
        object.put("data", array);
        return object.toJSONString();
    }

    /**
     * 按价格搜索商品
     * @param priceMin
     * @param priceMax
     * @return
     */
    @PostMapping("/search_commodity_price")
    @ResponseBody
    public String searchCommodityByPrice(String priceMin, String priceMax){
        List<Commodity> commodities = commodityService.getCommodityByPrice(priceMin, priceMax);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        array.addAll(commodities);
        object.put("count", commodities.size());
        object.put("data", array);
        return object.toJSONString();
    }

    /**
     * 按数量搜索商品
     * @param amountMin
     * @param amountMax
     * @return
     */
    @PostMapping("/search_commodity_amount")
    @ResponseBody
    public String searchCommodityByAmount(String amountMin, String amountMax){
        List<Commodity> commodities = commodityService.getCommodityByAmount(amountMin, amountMax);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        array.addAll(commodities);
        object.put("count", commodities.size());
        object.put("data", array);
        return object.toJSONString();
    }

    /**
     * 批量删除商品
     * @param map 前端JSON的键值对
     * @return 提示信息
     */
    @PostMapping("/delete_commodity")
    @ResponseBody
    public String deleteCommodity(@RequestParam Map<String, String> map){
        List<Integer> commoditiesId = new ArrayList<>();
        for(int i = 0; i < map.size(); i++){
            commoditiesId.add(Integer.parseInt(map.get(i+"")));
        }
        int i = commodityService.deleteCommoditiesById(commoditiesId);
        JSONObject object = new JSONObject();
        if (i > 0){
            object.put("code", 0);//成功
            object.put("msg", "success");//成功
            //删除对该商品的评论
            for (Integer integer : commoditiesId) {
                commentService.deleteCommentByTypeId(integer, 2);
            }
        }
        else{
            object.put("code", 1);//失败
            object.put("msg", "failure");//失败
        }
        return object.toJSONString();
    }

    /**
     * 添加商品
     * @param map 商品相关信息
     * @return {code: 0, msg: 'success'}
     */
    @PostMapping("/add_commodity")
    @ResponseBody
    public Map addCommodity(@RequestParam Map<String, String> map){
        Map<String, Object> result = new HashMap<>();
        Commodity commodity = new Commodity();

        Shop shop = new Shop();
        shop.setShopId(Integer.parseInt(map.get("shopId")));
        if (shopService.getShopByCondition(shop).size() > 0)
            commodity.setShopId(shop.getShopId());
        else{
            result.put("code", 2);
            result.put("msg", "店铺不存在");
            return result;
        }

        commodity.setCmodName(map.get("cmodName"));
        commodity.setCmodIntro(map.get("cmodIntro"));
        commodity.setType(map.get("type"));
        if (!map.get("amount").isEmpty())
            commodity.setAmount(Integer.parseInt(map.get("amount")));
        if (!map.get("sale").isEmpty())
            commodity.setSale(Integer.parseInt(map.get("sale")));
        if (!map.get("price").isEmpty())
            commodity.setPrice(Double.parseDouble(map.get("price")));
        commodity.setCmodImg(map.get("cmodImg"));

        int i = commodityService.addCommodity(commodity);
        if (i > 0){
            result.put("code", 0);
            result.put("msg", "success");
        }else{
            result.put("code", 1);
            result.put("msg", "failure");
        }
        return result;
    }

    /**
     * 编辑商品
     * @param map 商品相关信息
     * @return {code: 0, msg: 'success'}
     */
    @PostMapping("/update_commodity")
    @ResponseBody
    public Map updateCommodity(@RequestParam Map<String, String> map){
        Map<String, Object> result = new HashMap<>();
        Commodity commodity = new Commodity();
        commodity.setCmodId(Integer.parseInt(map.get("cmodId")));
        if (!map.get("shopId").equals("")){
            Shop shop = new Shop();
            shop.setShopId(Integer.parseInt(map.get("shopId")));
            if (shopService.getShopByCondition(shop).size() > 0)
                commodity.setShopId(shop.getShopId());
            else{
                result.put("code", 2);
                result.put("msg", "店铺不存在");
                return result;
            }
        }
        commodity.setCmodName(map.get("cmodName"));
        commodity.setCmodIntro(map.get("cmodIntro"));
        commodity.setType(map.get("type"));
        if (!map.get("amount").isEmpty())
            commodity.setAmount(Integer.parseInt(map.get("amount")));
        if (!map.get("sale").isEmpty())
            commodity.setSale(Integer.parseInt(map.get("sale")));
        if (!map.get("price").isEmpty())
            commodity.setPrice(Double.parseDouble(map.get("price")));
        commodity.setCmodImg(map.get("cmodImg"));

        int i = commodityService.updateCommodityById(commodity);
        if (i > 0){
            result.put("code", 0);
            result.put("msg", "success");
        }else{
            result.put("code", 1);
            result.put("msg", "failure");
        }
        return result;
    }
}
