package com.zcy.controller.manager;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zcy.entity.Manager;
import com.zcy.service.ManagerService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * (Manager)表控制层
 *
 * @author makejava
 * @since 2021-04-23 17:55:32
 */
@Controller
@RequestMapping("/manager")
public class ManagerController {
    @Resource
    private ManagerService managerService;

    /**
     * 分页查询管理员表
     * @param page 当前页
     * @param limit 页面大小
     * @return
     */
    @PostMapping("/get_manager")
    @ResponseBody
    public String getManager(int page, int limit){
        List<Manager> managers = managerService.getManagerByPage(page, limit);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        array.addAll(managers);
        object.put("count", managers.size());
        object.put("data", array);
        return object.toJSONString();
    }

    /**
     * 按条件搜索管理员
     * @param manager Manager的JSON
     * @return 返回管理员的json
     */
    @PostMapping("/search_manager")
    @ResponseBody
    public String searchManager(@RequestBody Manager manager){
        List<Manager> managers = managerService.getManagerByCondition(manager);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        array.addAll(managers);
        object.put("count", managers.size());
        object.put("data", array);
        return object.toJSONString();
    }

    /**
     * 批量删除管理员
     * @param map 前端JSON的键值对
     * @return 提示信息
     */
    @PostMapping("/delete_manager")
    @ResponseBody
    public String deleteManager(@RequestParam Map<String, String> map){
        List<Integer> managersId = new ArrayList<>();
        for(int i = 0; i < map.size(); i++){
            managersId.add(Integer.parseInt(map.get(i+"")));
        }
        int i = managerService.deleteManagersById(managersId);
        JSONObject object = new JSONObject();
        if (i > 0){
            object.put("code", 0);//成功
            object.put("msg", "success");//成功
        }
        else{
            object.put("code", 1);//失败
            object.put("msg", "failure");//失败
        }
        return object.toJSONString();
    }

    /**
     * 添加管理员
     * @param map 管理员相关信息
     * @return {code: 0, msg: 'success'}
     */
    @PostMapping("/add_manager")
    @ResponseBody
    public Map addManager(@RequestParam Map<String, String> map){
        Map<String, Object> result = new HashMap<>();
        Manager manager = new Manager();
        manager.setManagerName(map.get("managerName"));
        manager.setPassword(map.get("password"));
        manager.setAuthority(map.get("authority"));
        manager.setAvatar(map.get("avatar"));

        int i = managerService.addManager(manager);
        if (i > 0){
            result.put("code", 0);
            result.put("msg", "success");
        }else{
            result.put("code", 1);
            result.put("msg", "failure");
        }
        return result;
    }

    /**
     * 编辑管理员
     * @param map 管理员相关信息
     * @return {code: 0, msg: 'success'}
     */
    @PostMapping("/update_manager")
    @ResponseBody
    public Map updateManager(@RequestParam Map<String, String> map, HttpServletRequest request){
        Map<String, Object> result = new HashMap<>();
        Manager manager = new Manager();
        manager.setManagerId(Integer.parseInt(map.get("managerId")));
        manager.setManagerName(map.get("managerName"));
        manager.setPassword(map.get("password"));
        manager.setAuthority(map.get("authority"));
        manager.setAvatar(map.get("avatar"));

        int i = managerService.updateManagerById(manager);
        if (i > 0){
            result.put("code", 0);
            result.put("msg", "success");
            //如果是来自编辑个人信息的话，要更新session的值
            if (map.get("from")!=null && map.get("from").equals("cedit")){
                //更新个人信息
                HttpSession session = request.getSession();
                session.setAttribute("manager", manager);
            }
        }else{
            result.put("code", 1);
            result.put("msg", "failure");
        }
        return result;
    }
}
