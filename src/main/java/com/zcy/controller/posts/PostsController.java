package com.zcy.controller.posts;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zcy.entity.Posts;
import com.zcy.entity.Shop;
import com.zcy.entity.User;
import com.zcy.service.CommentService;
import com.zcy.service.PostsService;
import com.zcy.service.ShopService;
import com.zcy.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * (Posts)表控制层
 *
 * @author makejava
 * @since 2021-04-23 17:55:32
 */
@RestController
@RequestMapping("posts")
public class PostsController {
    @Resource
    private PostsService postsService;
    @Resource
    private UserService userService;
    @Resource
    private CommentService commentService;

    /**
     * 分页查询贴子表
     * @param page 当前页
     * @param limit 页面大小
     * @return
     */
    @PostMapping("/get_posts")
    @ResponseBody
    public String getPosts(int page, int limit){
        List<Posts> posts = postsService.getPostsByPage(page, limit);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        array.addAll(posts);
        object.put("count", posts.size());
        object.put("data", array);
        return object.toJSONString();
    }

    /**
     * 按条件搜索贴子
     * @param posts Posts的JSON
     * @return 返回贴子的json
     */
    @PostMapping("/search_posts")
    @ResponseBody
    public String searchPosts(@RequestBody Posts posts) throws ParseException {
        List<Posts> postsList = postsService.getPostsByCondition(posts);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        array.addAll(postsList);
        object.put("count", postsList.size());
        object.put("data", array);
        return object.toJSONString();
    }

    /**
     * 批量删除贴子
     * @param map 前端JSON的键值对
     * @return 提示信息
     */
    @PostMapping("/delete_posts")
    @ResponseBody
    public String deletePosts(@RequestParam Map<String, String> map){
        List<Integer> postsId = new ArrayList<>();
        for(int i = 0; i < map.size(); i++){
            postsId.add(Integer.parseInt(map.get(i+"")));
        }
        int i = postsService.deletePostsById(postsId);
        JSONObject object = new JSONObject();
        if (i > 0){
            object.put("code", 0);//成功
            object.put("msg", "success");//成功
            //删除对该贴子的评论
            for (Integer integer : postsId) {
                commentService.deleteCommentByTypeId(integer, 1);
            }
        }
        else{
            object.put("code", 1);//失败
            object.put("msg", "failure");//失败
        }
        return object.toJSONString();
    }

    /**
     * 添加贴子
     * @param map 贴子相关信息
     * @return {code: 0, msg: 'success'}
     */
    @PostMapping("/add_posts")
    @ResponseBody
    public Map addPosts(@RequestParam Map<String, String> map){
        Map<String, Object> result = new HashMap<>();
        Posts posts = new Posts();
        if (!map.get("authorId").equals("")){
            User user = new User();
            user.setUserId(Integer.parseInt(map.get("authorId")));
            if (userService.getUserByCondition(user).size() > 0)
                posts.setAuthorId(user.getUserId());
            else{
                result.put("code", 2);
                result.put("msg", "作者不存在");
                return result;
            }
        }

        posts.setPostTitle(map.get("postTitle"));
        posts.setPostContent(map.get("postContent"));
        if (!map.get("reward").isEmpty())
            posts.setReward(Double.parseDouble(map.get("reward")));
        posts.setPostImg(map.get("postImg"));

        int i = postsService.addPosts(posts);
        if (i > 0){
            result.put("code", 0);
            result.put("msg", "success");
        }else{
            result.put("code", 1);
            result.put("msg", "failure");
        }
        return result;
    }

    /**
     * 编辑贴子
     * @param map 店铺相关信息
     * @return {code: 0, msg: 'success'}
     */
    @PostMapping("/update_posts")
    @ResponseBody
    public Map updatePosts(@RequestParam Map<String, String> map){
        Map<String, Object> result = new HashMap<>();
        Posts posts = new Posts();
        posts.setPostId(Integer.parseInt(map.get("postId")));
        if (!map.get("authorId").equals("")){
            User user = new User();
            user.setUserId(Integer.parseInt(map.get("authorId")));
            if (userService.getUserByCondition(user).size() > 0)
                posts.setAuthorId(user.getUserId());
            else{
                result.put("code", 2);
                result.put("msg", "作者不存在");
                return result;
            }
        }
        posts.setPostTitle(map.get("postTitle"));
        posts.setPostContent(map.get("postContent"));
        if (map.get("reward") != null)
            posts.setReward(Double.parseDouble(map.get("reward")));
        posts.setPostImg(map.get("postImg"));

        int i = postsService.updatePostsById(posts);
        if (i > 0){
            result.put("code", 0);
            result.put("msg", "success");
        }else{
            result.put("code", 1);
            result.put("msg", "failure");
        }
        return result;
    }
}
