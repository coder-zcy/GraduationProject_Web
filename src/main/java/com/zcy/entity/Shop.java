package com.zcy.entity;

import java.io.Serializable;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * (Shop)表实体类
 *
 * @author makejava
 * @since 2021-04-23 17:55:32
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Shop implements Serializable {
    //编号
    @TableId(type = IdType.AUTO)
    private Integer shopId;
    //名称
    private String shopName;
    //介绍
    private String shopIntro;
    //店铺图片
    private String shopImg;
    //店铺关键词
    private String keywords;
    //店主ID号
    private Integer userId;
    //创建时间
    @JSONField(format="yyyy-MM-dd")
    private Date createTime;
    //更新时间
    @JSONField(format="yyyy-MM-dd")
    private Date updateTime;
    //乐观锁
    private Integer version;


}
