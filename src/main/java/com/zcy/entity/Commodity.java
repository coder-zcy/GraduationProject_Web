package com.zcy.entity;

import java.io.Serializable;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * (Commodity)表实体类
 *
 * @author makejava
 * @since 2021-04-23 17:55:31
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Commodity implements Serializable {
    //商品编号
    @TableId(type = IdType.AUTO)
    private Integer cmodId;
    //商品名
    private String cmodName;
    //商品介绍
    private String cmodIntro;
    //商品图片
    private String cmodImg;
    //商品数量
    private Integer amount;
    //商品销量
    private Integer sale;
    //商品价格
    private Double price;
    //商品类型
    private String type;
    //创建时间
    @JSONField(format="yyyy-MM-dd")
    private Date createTime;
    //更新时间
    @JSONField(format="yyyy-MM-dd")
    private Date updateTime;
    //所属店铺的编号
    private Integer shopId;
    //店主ID
    private Integer userId;
    //店铺名称
    private String shopName;
    //店主名称
    private String userName;
    //乐观锁
    private Integer version;
}
