package com.zcy.entity;

import java.io.Serializable;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * (Manager)表实体类
 *
 * @author makejava
 * @since 2021-04-23 17:55:32
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Manager implements Serializable {
    //账号
    @TableId(type = IdType.AUTO)
    private Integer managerId;
    //名称
    private String managerName;
    //密码
    private String password;
    //头像
    private String  avatar;
    //权限
    private String authority;
    //创建时间
    @JSONField(format="yyyy-MM-dd")
    private Date createTime;
    //更新时间
    @JSONField(format="yyyy-MM-dd")
    private Date updateTime;
    //乐观锁
    private Integer version;


}
