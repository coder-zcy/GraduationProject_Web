package com.zcy.entity;

import java.io.Serializable;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * (Comment)表实体类
 *
 * @author makejava
 * @since 2021-04-23 17:55:31
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comment implements Serializable{
    //评论编号
    @TableId(type = IdType.AUTO)
    private Integer cmenId;
    //评论内容
    private String cmenContent;
    //评论图片(如果是用户与用户间的交流，则评论图片是该评论发送者头像的URL)
    private String cmenImg;
    //创建时间
    @JSONField(format="yyyy-MM-dd")
    private Date createTime;
    //更新时间
    @JSONField(format="yyyy-MM-dd")
    private Date updateTime;
    //针对用户交流，0-未读，1-已读
    private Integer isRead;
    //发表评论的用户ID
    private Integer userId;
    //评论类型(0-user、1-post、2-commodity)
    private Integer type;
    //评论对象的ID
    private Integer id;
    //乐观锁
    private Integer version;
}
