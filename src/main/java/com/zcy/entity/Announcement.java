package com.zcy.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * (Announcement)表实体类
 *
 * @author makejava
 * @since 2021-04-23 17:55:31
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Announcement implements Serializable {
    //公告编号
    @TableId(type = IdType.AUTO)
    private Integer annoId;
    //公告标题
    private String annoTitle;
    //公告内容
    private String annoContent;
    //是否处理
    private Integer isDeal;
    //创建时间
    @JSONField(format="yyyy-MM-dd")
    private Date createTime;
    //发表公告的ID
    private Integer id;
    //发表公告的ID的类型(0-user、1-manager)
    private Integer type;
    //更新时间
    @JSONField(format="yyyy-MM-dd")
    private Date updateTime;
    //乐观锁
    private Integer version;
}