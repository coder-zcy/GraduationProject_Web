package com.zcy.entity;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * (User)表实体类
 *
 * @author makejava
 * @since 2021-04-23 17:55:33
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    //账号
    @TableId(type = IdType.AUTO)
    private Integer userId;
    //用户名
    private String userName;
    //密码
    private String password;
    //电话
    private String phoneNumber;
    //头像
    private String avatar;
    //等级
    private Integer point;
    //性别
    private Integer sex;
    //签名
    private String sign;
    //地址
    private String address;
    //学号
    private String studentId;
    //学校
    private String school;
    //年级
    private String grade;
    //创建时间
    @JSONField(format="yyyy-MM-dd")
    private Date createTime;
    //更新时间
    @JSONField(format="yyyy-MM-dd")
    private Date updateTime;
    //乐观锁
    private Integer version;
}
