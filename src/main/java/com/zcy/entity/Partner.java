package com.zcy.entity;

import java.io.Serializable;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * (UserShop)表实体类
 *
 * @author makejava
 * @since 2021-04-23 17:55:33
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Partner implements Serializable {
    //店主账号
    @TableId
    private Integer shopkeeperId;
    //店主伙伴1账号
    @TableField(updateStrategy=FieldStrategy.IGNORED)//使得更新时可以为空
    private Integer partner1Id;
    //店主伙伴2账号
    @TableField(updateStrategy=FieldStrategy.IGNORED)//使得更新时可以为空
    private Integer partner2Id;
    //店铺编号
    private Integer shopId;
    //创建时间
    @JSONField(format="yyyy-MM-dd")
    private Date createTime;
    //更新时间
    @JSONField(format="yyyy-MM-dd")
    private Date updateTime;
    //乐观锁
    private Integer version;


}
