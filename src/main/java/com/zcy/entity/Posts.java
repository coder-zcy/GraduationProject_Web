package com.zcy.entity;

import java.io.Serializable;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * (Posts)表实体类
 *
 * @author makejava
 * @since 2021-04-23 17:55:32
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Posts implements Serializable {
    //贴子编号
    @TableId(type = IdType.AUTO)
    private Integer postId;
    //标题
    private String postTitle;
    //内容
    private String postContent;
    //报酬
    private Double reward;
    //贴子图片
    private String postImg;
    //创建时间
    @JSONField(format="yyyy-MM-dd")
    private Date createTime;
    //更新时间
    @JSONField(format="yyyy-MM-dd")
    private Date updateTime;
    //发表贴子的用户ID
    private Integer authorId;
    //用户名
    private String authorName;
    //乐观锁
    private Integer version;


}
