package com.zcy.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zcy.entity.Partner;
import org.springframework.stereotype.Repository;

/**
 * (Partner)表数据库访问层
 *
 * @author makejava
 * @since 2021-04-23 17:55:33
 */
@Repository
public interface PartnerDao extends BaseMapper<Partner> {

}
