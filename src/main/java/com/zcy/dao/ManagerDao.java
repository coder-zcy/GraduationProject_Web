package com.zcy.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zcy.entity.Manager;
import org.springframework.stereotype.Repository;

/**
 * (Manager)表数据库访问层
 *
 * @author makejava
 * @since 2021-04-23 17:55:32
 */
@Repository
public interface ManagerDao extends BaseMapper<Manager> {

}
