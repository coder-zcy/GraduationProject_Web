package com.zcy.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zcy.entity.Shop;
import org.springframework.stereotype.Repository;

/**
 * (Shop)表数据库访问层
 *
 * @author makejava
 * @since 2021-04-23 17:55:32
 */
@Repository
public interface ShopDao extends BaseMapper<Shop> {


}
