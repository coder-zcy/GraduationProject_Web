package com.zcy.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zcy.entity.Commodity;
import org.springframework.stereotype.Repository;

/**
 * (Commodity)表数据库访问层
 *
 * @author makejava
 * @since 2021-04-23 17:55:31
 */
@Repository
public interface CommodityDao extends BaseMapper<Commodity> {

}
