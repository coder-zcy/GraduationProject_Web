package com.zcy.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zcy.entity.Announcement;
import org.springframework.stereotype.Repository;

/**
 * (AnnouncementDao)表数据库访问层
 *
 * @author makejava
 * @since 2021-04-23 17:55:32
 */
@Repository
public interface AnnouncementDao extends BaseMapper<Announcement> {
}
